# Arbitrage Bot

A cryptocurrency cross-exchange arbitrage bot. 


How it works: 

* We choose a pair of exchanges, one of which has high liquidity and low spreads (the *strong* exchange), and another which has high bid-ask spreads  (the *weak* exchange).
* On the weak exchange, the bot only places limit orders.
* When our limit order is hit, we place the opposite market order on the strong exchange.

An example transaction is shown below. 
![](images/exampleTransaction.png)
At ```08:39:26```, our limit order was hit on the weak exchange (BTCMarkets), and so at ```08:39:28```, we placed the opposite market order on the strong exchange (Binance). The difference in prices is approximately 1.7\%, and most of this is profit (fees are around 0.1\%).

### Screenshots

Output to terminal:

![](images/runningscreenshot.png) 

Config file (```cfltc.json```):


```json
{"exchange": {
    "configName": "cfLTC",
    "exchangeKeyRef": "coinfalconFiller",
    "exchangeAPIRef": "coinfalcon",
    "market": "LTC-BTC",
    "amountPrecision": 5,
    "pricePrecision": 5,
    "fee": 0.0025,
    "minOrderSize": 1e-5,
    "volumeRatio": 0.96,
    "e2market": "LTC-BTC",
    "checkOrders": true,
    "serial": false
    },
"mainLoop": {
"stop": false,
"pause": false,
"interval": 2
},
"binance": {
    "btc_use_ratio": 0.99,
    "market_sig_fig": 0,
    "fee": 0.0005,
    "market": "LTC-BTC"
},
"strategy": {
    "target_depths": {"buy": 0.005, "sell": 0.005},
    "thresholds": {"buy": 0.005, "sell": 0.005},
    "p_ratio_const": 1.4,
    "volume_ratio_cutoff": 1
}
}
```

The main loop (```mainLoop.py```):

```python
def mainLoopStep(self):
      # Make sure configs are up to date
      self._loadConfig(self.configFileName)
      self.marketController._loadConfig(self.configFileName)
      self.strategy._loadConfig(self.configFileName)
      self.strongExchange._loadConfig(self.configFileName)

      # Update our view of the weak exchange
      self.marketController.updateStep()

      # If any of our orders have been filled on the weak exchange,
      # add the corresponding closing action to the queue
      self.updateBuffer()

      # Close the outstanding amount on the strong exchange to realize profits
      self.strongExchange.clearBuffers()

      self.strongExchange.updateState()
      self.liquidityInfo = self.strongExchange.getLiquidityInfo()

      # Recompute the profitability of our open orders using new liquidity info
      self.updateProfitRatios()

      # Place/cancel orders on the weak exchange in line with our strategy
      self.updateActiveOrders(self.strategy)

      self.updateDatabase()

      self.numberOfLoops += 1
```

The ```self.strategy``` object takes the current state of the market (the order books on both the weak and strong exchange) and returns a pair of orders that we should open on the weak exchange.


### Notes

* To add a new weak exchange, you need to
    * Implement the exchange API, see for example ```source/exchangeClients/myNanex.py```. This object should have methods such as ```getOrderBooks()``` and ```getOpenOrders()```.
    * Add a new entry into ```source/util.py/makeClient()```
    * Add a new config file into ```configs```
    * Add your API keys to ```keys.json```
* Start the bot with ```python3 main.py $CONFIGPATH run```
