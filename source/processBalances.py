import databaseHandler
import time
import sys


exchangeList = ['cfETH', 'bigETH', 'kucETH', 'binance', 'btcmETH', 'cfLTC']
coin_list = ['BTC', 'XRB', 'ETH', 'LTC']


def get_last_balance(exchange, hours_ago):
    '''Returns the balance at a given time'''
    time1 = time.time() - hours_ago * 3600
    ebi = databaseHandler.ExchangeBalanceInfo
    return ebi.select().where(
                ebi.name == exchange,
                ebi.timestamp < time1
                ).order_by(ebi.timestamp.desc()).get()


def get_last_balances(hours_ago=0):
    balances = {}
    for exchange in exchangeList:
        balances[exchange] = vars(get_last_balance(exchange, hours_ago))['__data__']
    return balances


def get_total_balances(hours_ago=0):
    balances = get_last_balances(hours_ago)
    totals = {}
    for coin in coin_list:
        totals[coin] = sum([v[coin] for k, v in balances.items()])
    return totals


def specific_change(exchange, hours_ago_1, hours_ago_2=0):
    balance1 = vars(get_last_balance(exchange, hours_ago_1))['__data__']
    balance2 = vars(get_last_balance(exchange, hours_ago_2))['__data__']
    change = {}
    for coin, amt in balance1.items():
        if coin in coin_list:
            change[coin] = balance2[coin] - amt
    return change


def change_in_balance(hours_ago_1, hours_ago_2=0):
    balance1 = get_total_balances(hours_ago_1)
    balance2 = get_total_balances(hours_ago_2)
    change = {}
    for coin, amt in balance1.items():
        change[coin] = balance2[coin] - amt
    return change


# def discrepancyChecker(balance1, balance2):
#     '''balance1 and balance2 should be two consecutive balances
#        for the same exchange
#      '''
#     net_changes = netChange(balance1, balance2)
#     timestamps = (balance1['timestamp'], balance2['timestamp'])
#     if all([change < 0 for coin, change in net_changes.items()]):
#         return {'isDiscrepancy': True, 'timestamps': timestamps}
#     if all([change > 0 for coin, change in net_changes.items()]):
#         return {'isDiscrepancy': True, 'timestamps': timestamps}
#     if all():
#         pass

def checkForDiscrepancies(exchange, coinAAA, coinBBB, target,
                          hours_ago, end=time.time()):
    start = end - 3600 * hours_ago
    ebi = databaseHandler.ExchangeBalanceInfo
    balances = ebi.select().where(
            ebi.name == exchange,
            ebi.timestamp < end,
            ebi.timestamp > start
            ).order_by(ebi.timestamp.asc())
    discs = []
    for b1, b2 in zip(balances, balances[1:]):
        balance1 = getBalanceInfo(b1, coinAAA, coinBBB)
        balance2 = getBalanceInfo(b2, coinAAA, coinBBB)
        dCheck = discrepancyChecker(balance1, balance2, target)
        if dCheck['isDiscrepancy']:
            discs.append(dCheck)
    return discs


def main(hours_ago):
    for exchange in ['cfETH', 'bigETH', 'kucETH', 'btcmETH']:
        print("Exchange: "+ exchange)
        print([a['ratio'] for a in
        checkForDiscrepancies(exchange, 'ETH', 'BTC', 0.066, hours_ago)])

    print("Exchange: cfLTC")
    print([a['ratio'] for a in
        checkForDiscrepancies('cfLTC', 'LTC', 'BTC', 0.016, hours_ago)])
        
def getBalanceInfo(balance, coinAAA, coinBBB):
    return {'timestamp': getattr(balance, 'timestamp'),
            'AAA': getattr(balance, coinAAA),
            'BBB': getattr(balance, coinBBB)
            }


def discrepancyChecker(balance1, balance2, target):
    '''
    Balance1 and Balance2 is just the {AAA:, BBB:, timestamp:}
    target is ['buy': , 'sell':]
    '''
    net_changes = {}
    timestamps = (balance1['timestamp'], balance2['timestamp'])
    net_changes['AAA'] = balance2['AAA'] - balance1['AAA']
    net_changes['BBB'] = balance2['BBB'] - balance1['BBB']
    # Assuming we can get target price for our BBB, this is the gain
    est_BBB_change = net_changes['BBB'] + net_changes['AAA'] * target
    # Assuming we can get target price for our AAA, this is the gain.
    # Problem: What if we sold AAA for really expensive? still fine.
    if est_BBB_change < -0.00001:
        if net_changes['AAA'] == 0:
            net_changes['AAA'] = 0.0000001
        ratio = net_changes['BBB'] / net_changes['AAA']
        return {'isDiscrepancy': True, 'timestamps': timestamps,
                'change': net_changes, 'ratio': ratio
                }
    return {'isDiscrepancy': False, 'timestamps': [],
            'change': net_changes}


def netChange(balance1, balance2):
    net_change = {}
    for coin in coin_list:
        net_change[coin] = balance2[coin] - balance1[coin]


# def netChange(trade):
#     price = float(trade['price'])
#     amount = float(trade['size'])
#     if trade['side'] == 'buy':
#         return [price * amount, -amount]
#     if trade['side'] == 'sell':
#         return [-price * amount, amount]
# dbtc = 0
# dltc = 0
# for trade in alltrades:
#     dx, dy = netChange(trade)
#     dbtc += dx
#     dltc += dy

if __name__ == "__main__":
    hours_ago = float(sys.argv[1])
    print(get_total_balances(hours_ago))
