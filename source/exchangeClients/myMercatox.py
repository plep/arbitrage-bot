import time


class MercatoxClient():

    def __init__(self, rawClient, market):
        '''Only pairs are NANO-XXX
            We provide a XRB-XXX interface.
        '''
        self.rawClient = rawClient
        self.lastUpdate = None
        self.market = market
        self.AAA = self.market[:3]
        self.BBB = self.market[-3:]
        self.seenTradeIds = []
        self.last_refresh = time.time()
        self.max_time_since_refresh = 300  # 30 minutes

    def getOrderBooks(self):
        self.keep_alive()

        rawResponse = self.rawClient.getBooks()
        buyBook = [[float(price), float(amt)]
                   for price, amt, vol in rawResponse['buy']]
        sellBook = [[float(price), float(amt)]
                    for price, amt, vol in rawResponse['sell']]
        return {'buy': buyBook, 'sell': sellBook}

    def keep_alive(self):
        if time.time() - self.last_refresh > self.max_time_since_refresh:
            self.rawClient.refreshBrowsers()
            self.last_refresh = time.time()

    def getOpenOrders(self):
        rawResponse = self.rawClient.getOrders()
        orders = []
        for rawOrder in rawResponse:
            order = {}
            order['side'] = rawOrder['side']
            order['price'] = float(rawOrder['price'])
            orders.append(order)
        return orders

    def getBalances(self):
        '''Input is dictionary from quadrigaClient.get_balance()'''
        return self.rawClient.getBalances()

    def getNewTrades(self):
        newTrades = []
        # only want trades (type = 2)
        rawResponse = self.rawClient.getFills()
        for entry in rawResponse:
            id = entry['id']
            if id not in self.seenTradeIds:
                self.seenTradeIds.append(id)
                trade = self.interpretTrade(entry)
                newTrades.append(trade)
        return newTrades

    def interpretTrade(self, entry):
        ''''''
        trade = {}
        trade['id'] = entry['id']
        trade['market'] = self.market
        trade['price'] = float(entry['price'])
        trade['amount'] = float(entry['amount'])
        trade['timestamp'] = entry['timestamp']
        trade['side'] = entry['side']
        return trade

    def placeOrder(self, order):
        if order.side == 'sell':
            return self.rawClient.placeSell(
                             str(order.price), str(order.amount))
        elif order.side == 'buy':
            return self.rawClient.placeBuy(
                             str(order.price), str(order.amount))
        else:
            raise TypeError(f'Order must buy or sell, got {order.side}')

    def cancelOrder(self, order):
        return self.rawClient.cancelSideOrders(order.side)