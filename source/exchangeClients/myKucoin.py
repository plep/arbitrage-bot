import time
import exceptions
import kucoin


class KucoinClient():

    def __init__(self, rawClient, market):
        self.rawClient = rawClient
        self.lastUpdate = None
        self.market = market
        self.AAA = self.market[:3]
        self.BBB = self.market[-3:]

        self.seenTradeIds = []
        self.lastCall = time.time()
        self.delay = 0.5  # 1 second between each API call
        # Load up our past trades

    def getOrderBooks(self):
        return self.slowRequest(self._getOrderBooks)

    def getBalances(self):
        return self.slowRequest(self._getBalances)

    def getOpenOrders(self):
        return self.slowRequest(self._getOpenOrders)

    def getNewTrades(self):
        return self.slowRequest(self._getNewTrades)

    def placeOrder(self, order):
        return self.slowRequest(self._placeOrder, order)

    def cancelOrder(self, order):
        return self.slowRequest(self._cancelOrder, order)

    def _getOrderBooks(self):
        rawResponse = self.rawClient.get_order_book(self.market, limit=50)
        buyBook = [entry[:2] for entry in rawResponse['BUY']]
        sellBook = [entry[:2] for entry in rawResponse['SELL']]
        return {'buy': buyBook, 'sell': sellBook}

    def _getBalances(self):
        '''Input is dictionary from quadrigaClient.get_balance()'''
        rawResponse = self.rawClient.get_all_balances()
        balances = {}
        for entry in rawResponse:
            currencyName = entry['coinType']
            data = {}
            data['reserved'] = entry['freezeBalance']
            data['total'] = entry['balance'] + data['reserved']
            data['free'] = entry['balance']
            balances[currencyName] = data
        return balances

    def _getOpenOrders(self):
        orders = []
        rawResponse = self.rawClient.get_active_orders(
                            self.market)
        rawResponse = rawResponse['SELL']+rawResponse['BUY']
        for entry in rawResponse:
            order = {}
            order['id'] = entry[-1]
            order['price'] = entry[2]
            order['market'] = self.market
            order['amount'] = entry[3]-entry[4]
            if entry[1] == 'BUY':
                order['side'] = 'buy'
            if entry[1] == 'SELL':
                order['side'] = 'sell'
            order['timestamp'] = entry[0]
            orders.append(order)
        return orders

    def _getNewTrades(self):
        '''Gets trades since self.lastUpdate and update self.lastUpdate '''
        newTrades = []
        if self.lastUpdate is None:
            rawResponse = self.rawClient.get_dealt_orders(
                            symbol=self.market)['datas']
        else:
            rawResponse = self.rawClient.get_dealt_orders(
                symbol=self.market, since=self.lastUpdate+1, limit=20)['datas']
        for entry in rawResponse:
            trade = {}
            trade['timestamp'] = entry['createdAt']
            trade['amount'] = entry['amount']
            trade['price'] = entry['dealPrice']
            trade['market'] = self.market
            trade['id'] = entry['oid']
            trade['fee'] = entry['fee']
            trade['side'] = entry['direction'].lower()
            newTrades.append(trade)
        if newTrades:
            self.lastUpdate = newTrades[0]['timestamp']
        if len(newTrades) >= 20:
            raise Exception("Too many newTrades")
        return newTrades

    def _placeOrder(self, order):
        try:
            if order.side == 'sell':
                data = self.rawClient.create_sell_order(self.market,
                                                        str(order.price),
                                                        str(order.amount))
            elif order.side == 'buy':
                data = self.rawClient.create_buy_order(self.market,
                                                       str(order.price),
                                                       str(order.amount))
            else:
                raise TypeError(f'Order must buy or sell, got {order.side}')

        except kucoin.exceptions.KucoinAPIException as ee:
                raise exceptions.InsufficientBalanceError(
                    str(ee)
                    )
        except KeyError as e:
            try:
                coin = data['coinType']
                raise exceptions.InsufficientBalanceError(
                    f'Insufficient {coin} to place order')
            except KeyError:
                raise e
        else:
            return {'id': data['orderOid']}

    def _cancelOrder(self, order):
        self.rawClient.cancel_all_orders(
                            symbol=self.market, order_type=order.side.upper())

    def slowRequest(self, func, order=None):
        timeTillNextReq = self.lastCall + self.delay - time.time()
        if timeTillNextReq > 0:
            time.sleep(timeTillNextReq)
        self.lastCall = time.time()
        if order is None:
            return func()
        else:
            return func(order)