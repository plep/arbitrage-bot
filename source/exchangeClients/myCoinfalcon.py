# Parsing functions for raw json coinfalon API responses
from .rawAPIs.coinFalcon import ResponseError
import exceptions
from collections import deque


class cfClient:

    def __init__(self, rawClient, market):
        '''client must be a coinfalconRaw client '''
        self.rawClient = rawClient
        self.lastUpdate = None
        self.market = market

        # Used to determine if trade is maker or taker
        self.order_ids = {'sell': deque(range(10), 10),
                          'buy': deque(range(10), 10)}

    def slashMarket(market):
        return market[:3]+'/'+market[-3:]

    def dashMarket(market):
        return market[:3]+'-'+market[-3:]

    def getBalances(self):
        '''Input is dictionary from coinFalcon.getBalance'''
        return self.rawClient._getBalances()
        # balances = {}
        # for entry in jsonResponse:
        #     currency = entry['currency_code'].upper()
        #     data = {}
        #     data['free'] = float(entry['available_balance'])
        #     data['total'] = float(entry['balance'])
        #     balances[currency] = data
        # print(balances)
        # return balances

    def getOpenOrders(self):
        '''self.orders becomes a list of dicts
        Each dict has the following keys:
        id
        market -- e.g. 'BTC-EUR'
        price -- float
        amount -- float
        side -- 'buy' or 'sell'
        timestamp -- ISO8601 string
        '''
        # logging.info('Looking for new trades...')
        '''Updates orders.'''
        market = self.market
        jsonOpen = self.rawClient._getOrders(
                              {'status': 'open', 'market': market})
        # jsonPartiallyFilled = self.rawClient._getOrders(
        #                       {'status': 'partially_filled', 'market': market})
        jsonPartiallyFilled = []
        orders = []
        for entry in jsonOpen+jsonPartiallyFilled:
            order = {}
            order['id'] = entry['id']
            market = entry['market']
            order['market'] = market
            order['side'] = entry['order_type']
            order['price'] = float(entry['price'])
            order['amount'] = (float(entry['size']) -
                               float(entry['size_filled']))
            orders.append(order)
        return orders

    def getOrderBooks(self):
        jsonBook = self.rawClient.getBook(self.market, 2)
        sellBook = []
        buyBook = []
        for entry in jsonBook['asks']:
            price, amount = float(entry['price']), float(entry['size'])
            order = price, amount
            sellBook.append(order)
        for entry in jsonBook['bids']:
            price, amount = float(entry['price']), float(entry['size'])
            order = price, amount
            buyBook.append(order)

        bookPair = {'buy': buyBook, 'sell': sellBook}
        return bookPair

    def getNewTrades(self):
        if self.lastUpdate is None:
            jsonTrades = self.rawClient._getTrades({'market': self.market})
        else:
            jsonTrades = self.rawClient._getTrades(
                        {'market': self.market, 'since_time': self.lastUpdate})
        trades = []
        for entry in jsonTrades:
            trade = {}
            trade['id'] = entry['id']
            trade['market'] = entry['market_name']
            trade['price'] = float(entry['price'])
            trade['amount'] = float(entry['size'])
            trade['side'] = self.real_side(entry)
            trade['timestamp'] = entry['created_at']  # ISO8601
            trades.append(trade)
        if trades:
            self.lastUpdate = trades[0]['timestamp']
        return trades

    def real_side(self, entry):
        # isMaker = entry['order_right_id'] in self.order_ids
        isMaker = True
        entrySide = entry['side']
        if isMaker:
            if entrySide == 'buy':
                return 'sell'
            if entrySide == 'sell':
                return 'buy'
        else:
            return entrySide

    def test_is_maker(self, entry):
        with open('left_ids.log', 'a') as f:
            f.write(entry['order_left_id']+'\n')
        with open('right_ids.log', 'a') as f:
            f.write(entry['order_right_id']+'\n')

        isMaker = entry['order_right_id'] in (self.order_ids['buy'] +
                                              self.order_ids['sell'])

        with open('is_maker_test.log', 'a') as f:
            tradeString = str(entry)
            if isMaker:
                f.write(f'{tradeString} is a Maker')
            else:
                f.write(f'{tradeString} is a Taker')
            f.write('\n')

    def placeOrder(self, order):
        try:
            orderResponse = self.rawClient.createOrder(
                        order.market, order.side, order.price, order.amount)
            order_id = orderResponse['id']
            with open('order_ids.log', 'a') as f:
                f.write(order_id+'\n')
        except ResponseError as e:
            if 'you don\'t have enough' in str(e):
                raise exceptions.InsufficientBalanceError(str(e))
        else:
            self.order_ids[order.side].append(order_id)
            # return orderResponse
            return orderResponse

    def cancelOrder(self, order):
        return self.rawClient.cancelOrder(order.id)
