from selenium import webdriver
import pyotp
import time
import json
import concurrent.futures
import logging
import deathbycaptcha
import asyncio

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

from bs4 import BeautifulSoup
LOGIN_URL = 'https://mercatox.com/auth'
LOGIN_BUTTON = '//*[@id="w0"]/input[2]'

EMAIL_FIELD = 'client-email'
PASSWD_FIELD = 'client-password'


class rawMercatoxClient():
    def __init__(self, market,
                 username='',
                 password='',
                 twoFA=''):

        self.config_file = "driver_config.json"
        self._loadConfig()
        self.market = market

        self.twoFA = twoFA
        self.username = username
        self.password = password

        self.browser = self.makeBrowser()
        self.balanceBrowser = self.makeBrowser()
        self.fillBrowser = self.makeBrowser()
        self.login([self.browser, self.balanceBrowser, self.fillBrowser])
        # marketUrl = 'https://mercatox.com/exchange/'+self.market[:3]+'/'+ self.market[-3:]
        # self.browser.get(marketUrl)
        # self.findBalanceIndices()

    def login(self, browsers):
        "logs in the browsers and navigates to the correct pages"
        for browser in browsers:
            browser.get(LOGIN_URL)

        time.sleep(20)
        for browser in browsers:
            self.enter_login_info(browser)

        # for browser in browsers:
        #     token = self.getCaptchaToken()
        #     browser.execute_script(f"document.getElementById('g-recaptcha-response').innerHTML = '{token}'  ")

        self.enterCaptchasParallel()
        time.sleep(1)
        for browser in browsers:
            self.click_login_and_enter_2fa(browser)

        
        status = input("Navigate to the right pages? ")
        while status != '':
            browser = browsers[int(status)]
            self.enter_login_info(browser)
            self.click_login_and_enter_2fa(browser)
            status = input("Navigate to the right pages? ")

        self.refreshBrowsers()

    def enterCaptchasParallel(self):
        '''Updates from client and client.'''
        # Due to API rate limits, call this at most once per second
        loop = asyncio.get_event_loop()
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=4)
        loop.run_until_complete(self._enterCaptchas(loop, executor))
        print("Captchas all entered")

    async def _enterCaptchas(self, loop, executor):
        await asyncio.wait(
            fs={
                loop.run_in_executor(None, lambda: self.getTokenAndSetInnerHTML(self.browser)),
                loop.run_in_executor(None, lambda: self.getTokenAndSetInnerHTML(self.fillBrowser)),
                loop.run_in_executor(None, lambda: self.getTokenAndSetInnerHTML(self.balanceBrowser))
            },
            return_when=asyncio.ALL_COMPLETED
            )

    def getTokenAndSetInnerHTML(self, browser):
        token = None
        while token is None:
            token = self.getCaptchaToken()
            print(f"Token = {token}")

        browser.execute_script(f"document.getElementById('g-recaptcha-response').innerHTML = '{token}'  ")

    def enter_login_info(self, browser):
        emailField = browser.find_element_by_id(EMAIL_FIELD)
        passwordField = browser.find_element_by_id(PASSWD_FIELD)
        emailField.clear()
        emailField.send_keys(self.username)
        passwordField.clear()
        passwordField.send_keys(self.password)

    def click_login_and_enter_2fa(self, browser):
        loginButton = browser.find_element_by_xpath(LOGIN_BUTTON)
        loginButton.click()
        time.sleep(3)
        code = pyotp.TOTP(self.twoFA).now()
        browser.switch_to.active_element.send_keys(code)
        time.sleep(0.5)

    def _loadConfig(self):
        config = json.load(open(self.config_file))
        self.DRIVERPATH = config['DRIVERPATH']
        self.CHROMEPATH = config['CHROMEPATH']

    def refreshBrowsers(self):
        # self.browser.refresh()
        # self.browser.find_element_by_xpath('//*[@id="buy-button"]').click()
        self.mainblocks = self.browser.find_elements_by_class_name("mainblock_body")
        self.findBalanceIndices()

    def makeBrowser(self):
        options = webdriver.ChromeOptions()
        options.add_argument("--window-size=1920x800")
        options.add_argument('--disable-extensions')
        # options.add_argument('--headless')
        options.add_argument('--disable-gpu')
        options.add_argument('--no-sandbox')
        options.add_argument("--start-maximized")
        options.binary_location = self.CHROMEPATH
        driver = webdriver.Chrome(chrome_options=options,
                                executable_path=self.DRIVERPATH)  # noqa
        return driver

    def getCaptchaToken(self, keyFile="keys.json"):
        logging.info('Getting captcha token...')
        NANEX_SITE_KEY = "6LdjWRsUAAAAAFmmJDOTmrpCcaZFioJZjUHO7jQD"
        NANEX_URL = "https://mercatox.com/auth"

        siteKey = NANEX_SITE_KEY
        siteUrl = NANEX_URL
        keys = json.load(open(keyFile))[-1]
        username = keys['username']
        password = keys['password']
    # Put the proxy and reCaptcha token data
        Captcha_dict = {
            'proxytype': 'HTTP',
            'googlekey': siteKey,
            'pageurl': siteUrl}

        json_Captcha = json.dumps(Captcha_dict)

        client = deathbycaptcha.HttpClient(username, password)

        try:
            # balance = client.get_balance()
            # Put your CAPTCHA type and Json payload here:
            captcha = client.decode(type=4, token_params=json_Captcha)
            if captcha:
                    # The CAPTCHA was solved; captcha["captcha"] item holds its
                # numeric ID, and captcha["text"] item its a text token".
                logging.info('Captcha received')
                return captcha["text"]

                if '':  # check if the CAPTCHA was incorrectly solved
                    client.report(captcha["captcha"])
        except deathbycaptcha.AccessDeniedException:
            # Access to DBC API denied, check your credentials and/or balance
            print("error: Access to DBC API denied, check your cred or balance")

    def getBooks(self):
        soup = BeautifulSoup(self.browser.page_source, 'lxml')
        mainblocks = soup.find_all('div', attrs={'class': 'mainblock_body'})
        sellSoup = mainblocks[3]
        buySoup = mainblocks[4]
        sellBook = self.processBook(sellSoup)
        buyBook = self.processBook(buySoup)
        # sellBookElement = self.mainblocks[3].find_element_by_class_name('mCSB_container')
        # buyBookElement = self.mainblocks[4].find_element_by_class_name('mCSB_container')
        # sellBook = self.processBook(sellBookElement)
        # buyBook = self.processBook(buyBookElement)
        return {'buy': buyBook, 'sell': sellBook}

    def processBook(self, bookSoup):
        # first row is header
        rows = bookSoup.find_all('div', attrs={'class': 'row'})[1:30]
        book = []
        for row in rows:
            cols = row.find_all('div', attrs={'class': 'col-xs-4'})
            book.append([cols[1].text, cols[0].text, cols[2].text])
        return book

    # def processBook(self, book):
    #     soup = BeautifulSoup(book.page_source, 'lxml')
    #     rows = soup.find_all('div', attrs={'class': 'row'})
    #     rows = book.find_elements_by_class_name('row')[:30]
    #     return [[float(e.get_attribute('price') ), float(e.get_attribute('amount'))] for e in rows]

    def getBalances(self):
        # balanceUrl = 'https://mercatox.com/wallet'
        # self.balanceBrowser.get(balanceUrl)

        self.browser.find_element_by_xpath("/html/body/div[1]/div[1]/div/div/div/div[1]/div[1]/div").click()
        time.sleep(3)

        self.balanceBrowser.find_element_by_xpath("/html/body/div[1]/div[1]/div/div/div/div[2]/div/div/div[4]/div/div[1]/div/div[3]/div/div[1]").click()

        walletElement = self.balanceBrowser.find_element_by_class_name('currencies_main_container')
        rows = walletElement.find_elements_by_class_name('currency_row')
        parsedRows = [self.parseBalanceRow(row) for i, row in enumerate(rows) if i in self.balanceIndices]
        coins = [row['coin'] for row in parsedRows]
        balanceInfos = [{'free': row['free'], 'total': row['total']} 
                        for row in parsedRows]
        return dict(zip(coins, balanceInfos))

    def parseBalanceRow(self, row):
        cols = row.find_elements_by_class_name('col-xs-2')
        rowData = {}
        rowData['coin'] = cols[0].text[:3]
        rowData['free'] = float(cols[1].text)
        rowData['reserved'] = float(cols[2].text)
        rowData['total'] = rowData['free']+rowData['reserved']
        return rowData

    def findBalanceIndices(self):
    #     balanceUrl = 'https://mercatox.com/wallet'
    #     self.balanceBrowser.get(balanceUrl)
        
        # input("Wait")
        self.balanceBrowser.find_element_by_xpath("/html/body/div[1]/div[1]/div/div/div/div[2]/div/div/div[4]/div/div[1]/div/div[3]/div/div[1]").click()
        time.sleep(3)
        walletElement = self.balanceBrowser.find_element_by_class_name('currencies_main_container')
        rows = walletElement.find_elements_by_class_name('currency_row')
        self.balanceIndices = []
        for i,row in enumerate(rows):
            try:
                self.parseBalanceRow(row)
            except ValueError:
                pass
            else:
                self.balanceIndices.append(i)


    def getFills(self):
        # fillUrl = 'https://mercatox.com/wallet/transactions'
        # self.fillBrowser.get(fillUrl)

        self.fillBrowser.find_element_by_xpath("/html/body/div[1]/div[1]/div/div/div/div[1]/a[1]/div/div").click()
        time.sleep(3)

        soup = BeautifulSoup(self.fillBrowser.page_source, 'lxml')
        fillRows = soup.find('table').find_all('tr')[2:]
        # first 2 rows are headers
        fills = [self.parseFillText(fillRow) for fillRow in fillRows]
        return [x for x in fills if x['isTrade']]

    def parseFillText(self, fillRow):
        cols = [c.text.strip(' ') for c in fillRow.find_all('td')]
        fillData = {}
        fillData['id'] = cols[0].strip('\n')
        fillData['market'] = cols[3]
        fillData['isTrade'] = (fillData['market'][:3] == self.market[:3] and
                               fillData['market'][-3:] == self.market[-3:])
        fillData['timestamp'] = cols[5]
        if fillData['isTrade']:
            parsedCell = self.parseFillCell(cols[1])
            fillData['amount'] = parsedCell['amount']
            fillData['price'] = parsedCell['price']
            fillData['volume'] = parsedCell['volume']
        fillData['side'] = cols[4].split(' ')[-1]
        return fillData

    # def getFills(self):
    #     fillTable = self.fillBrowser.find_element_by_class_name('table')
    #     fillRows = fillTable.find_elements_by_tag_name('tr')[2:]
    #     # first 2 rows are headers
    #     fills = [self.parseFillText(fillRow) for fillRow in fillRows]
    #     return [x for x in fills if x['isTrade']]

    # def parseFillText(self, fillRow):
    #     cols = [c.text for c in fillRow.find_elements_by_tag_name('td')]
    #     fillData = {}
    #     fillData['id'] = cols[0]
    #     fillData['market'] = cols[3]
    #     fillData['isTrade'] = fillData['market'] == self.market[:3] + ' / ' + self.market[-3:]
    #     fillData['timestamp'] = cols[5]
    #     if fillData['isTrade']:
    #         parsedCell = self.parseFillCell(cols[1])
    #         fillData['amount'] = parsedCell['amount']
    #         fillData['price'] = parsedCell['price']
    #         fillData['volume'] = parsedCell['volume']
    #     return fillData

    def parseFillCell(self, cell):
        textList = cell.split(':')
        data = {}
        data['amount'] = textList[1].split(' ')[1]
        data['price'] = textList[2].split(' ')[1]
        data['volume'] = textList[3].split(' ')[1]
        return data

    def getOrders(self):
        self.mainblocks = self.browser.find_elements_by_class_name("mainblock_body")
        orderContainer = self.mainblocks[0]
        orderRows = orderContainer.find_elements_by_class_name('row')[1:]
        orders = [self.parseOrderRow(orderRow) for orderRow in orderRows]
        return orders

    def parseOrderRow(self, orderRow):
        orderData = {}
        orderData['cancelButton'] = orderRow.find_elements_by_tag_name('div')[-1]
        cols = orderRow.text.split('\n')
        orderData['price'] = cols[1]
        orderData['totalAmount'] = cols[2]
        side = cols[0]
        if side == 'Buy':
            orderData['side'] = 'buy'
        elif side == 'Sell':
            orderData['side'] = 'sell'
        return orderData

    def placeBuy(self, price, amount):
        priceField = self.browser.find_element_by_id('buy-price')
        amountField = self.browser.find_element_by_id('buy-amount')
        priceField.clear()
        amountField.clear()
        priceField.send_keys(price)
        amountField.send_keys(amount)
        placeButton = self.browser.find_element_by_id('buy-button')
        placeButton.click()
        return {'id': 'No ID for MERCATOX Exchange '}

    def placeSell(self, price, amount):
        priceField = self.browser.find_element_by_id('sell-price')
        amountField = self.browser.find_element_by_id('sell-amount')
        priceField.clear()
        amountField.clear()
        priceField.send_keys(price)
        amountField.send_keys(amount)
        placeButton = self.browser.find_element_by_id('sell-button')
        placeButton.click()
        return {'id': 'No ID for MERCATOX Exchange '}

    def cancelSideOrders(self, side):
        orders = self.getOrders()
        for order in orders:
            if order['side'] == side:
                order['cancelButton'].click()
                try:
                    WebDriverWait(self.browser, 3).until(EC.alert_is_present(),
                                        'Timed out waiting for PA creation ' +
                                         'confirmation popup to appear.')
                    alert = self.browser.switch_to.alert
                    alert.accept()
                except TimeoutException:
                    print("no alert")
        return None

    def tearDown(self):
        self.browser.quit()
        self.balanceBrowser.quit()
