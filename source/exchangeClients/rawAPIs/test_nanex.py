import nanex
import mercatox
import sys
import pickle


def loadCookies(browser, file="cookies.pkl"):
    cookies = pickle.load(open("cookies.pkl", "rb"))
    for cookie in cookies:
        browser.add_cookie(cookie)


def dumpCookies(browser, file="cookies.pkl"):
    pickle.dump(browser.get_cookies(), open("cookies.pkl", "wb"))


def tearDown():
    client.browser.quit()
    client.balanceBrowser.quit()
    exit()


exchange = sys.argv[1]

if exchange == 'mercatox':
    client = mercatox.Client()
elif exchange == 'nanex':
    client = nanex.rawNanexClient('NANOBTC',
                          username='',
                          password='')

