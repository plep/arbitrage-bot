# Parsing functions for raw json coinfalon API responses


def balance(jsonResponse):
    '''Input is dictionary from coinFalcon.getBalance'''
    balances = {}
    for entry in jsonResponse:
        currency = entry['currency_code'].upper()
        data = {}
        data['free'] = float(entry['available_balance'])
        data['total'] = float(entry['balance'])
        balances[currency] = data
    return balances


def orders(jsonResponse):
    orders = []
    for entry in jsonResponse:
        order = {}
        order['id'] = entry['id']
        market = entry['market']
        order['market'] = market
        order['side'] = entry['order_type']
        order['price'] = float(entry['price'])
        order['amount'] = (float(entry['size']) -
                           float(entry['size_filled']))
        order['timestamp'] = entry['created_at']  # ISO8601
        if order['market'] == self.market:
            orders.append(order)
