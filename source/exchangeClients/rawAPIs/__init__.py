from .coinFalcon import rawCoinfalconClient
from .nanex import rawNanexClient
from .mercatox import rawMercatoxClient