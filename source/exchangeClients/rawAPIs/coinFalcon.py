import requests
import urllib
import urllib.parse
import hmac
import time
import hashlib
import json
import logging
from . import cfParsers


def responseParser(response):
    status_code = response.status_code
    if status_code == 200 or 201:  # 201 is successful post?
        # successful
        try:
            return response.json()['data']
        except KeyError:
            try:
                errorMessage = response.json()['error']
            except Exception:
                logging.exception(str(response.json()))
    elif status_code == 400:
        errorMessage = response.json()['error']
        raise RequestError(errorMessage)
    elif status_code == 401:
        errorMessage = response.json()['error']
        raise RequestError(errorMessage)
    elif status_code == 429:
        logging.warn('Throttle Limit Exceeded!')
        errorMessage = response.json()['error']
        raise RateLimitExceededError('error')
    elif status_code == 500:
        raise RequestError("500 Coinfalcon Internal Server Error")
    elif status_code == 404:
        raise RequestError("404 Not Found")
    elif status_code == 403:
        raise RequestError("403 Forbidden")
    else:
        jsonResponse = response.json()
        errorMessage = str([v for v in jsonResponse.items()])
        raise RequestError(errorMessage)


class Error(Exception):
    '''Base class for exceptions in this coinFalcon_public'''
    pass


class RateLimitExceededError(Error):
    '''Throttle limit exceeded'''
    def __init__(self, message):
        self.message = message


class ResponseError(Error):
    '''Request was successful, but server returned error'''
    def __init__(self, message):
        self.message = message


class RequestError(Error):
    '''Exception raised for failed coinFalcon request'''
    def __init__(self, message):
        self.message = message


class rawCoinfalconClient():

    def __init__(self, key, secret):
        self.endpoint = "https://coinfalcon.com/api/v1/"
        self.key = key
        self.secret = secret

    # def _requestPath(self, url):
    #     query = urllib.parse.urlparse(url).query
    #     if query == '':
    #         return urllib.parse.urlparse(url).path
    #     return urllib.parse.urlparse(url).path + '?' + query
    def _getQuery(self, queryUrl):
        response = requests.get(queryUrl)
        return responseParser(response)

    def getBook(self, market, level):
        '''Gets order book.

        Arguments:
        market -- e.g. "LTC-BTC"
        level -- 1 for only top bid/ask
                 2 for top 50 bid asks (aggregated)
                 3 for all bid asks (not aggregrated)

        Example output: output[0]['data'] is a dict with two keys,
                       'bids' and 'asks'.
        [{
            "bids": [
                {
                    "price": "6225.0",
                    "size": "0.01"
                },
                {
                    "price": "6220.14",
                    "size": "1.31"
                }
            ],
            "asks": [
                {
                    "price": "6400.0",
                    "size": "0.38493686"
                },
                {
                    "price": "6400.07",
                    "size": "0.0303693"
                }
            ]
            }]
        '''
        queryUrl = self.endpoint + f"markets/{market}/orders/?level={level}"
        return self._getQuery(queryUrl)

    def persistentGetBooks(self, market, level, retries):
        '''
        '''
        for i in range(retries):
            try:
                return self._getBook(self, market, level)
            except RateLimitExceededError:
                time.sleep(1)

    def _getHeaders(self, method, requestPath, body={}):
        timestamp = str(int(time.time()))

        if method == 'GET':
            payload = '|'.join([timestamp, method, requestPath])
        else:
            payload = '|'.join([timestamp, method, requestPath,
                                json.dumps(body, separators=(',', ':'))])
        logging.debug(payload)
        message = bytes(payload, 'utf-8')
        secret = bytes(self.secret, 'utf-8')

        signature = hmac.new(secret, message, hashlib.sha256).hexdigest()
        return {
            "CF-API-KEY": self.key,
            "CF-API-TIMESTAMP": timestamp,
            "CF-API-SIGNATURE": signature
        }

    def _createOrder(self, params):
        '''Creates order.

        Body has the following required parameters:
        market -- e.g. BTC-EUR
        operation_type --  market_order or limit_order
        order_type -- buy or sell the left currency of the market
        size -- set the size of the order
        price -- set price for limit_order (optional)

        Example output:

        [{
            "id": "4cc9835d-3aad-4e3b-aa76-538e6f18247a",
            "market": "ETH-BTC",
            "price": "0.03993",
            "size": "1.0",
            "size_filled": "0.0",
            "status": "pending",
            "order_type": "buy",
            "operation_type": "limit_order",
            "created_at": "2017-11-03T08:46:14.354945Z"
        }
        }]
        '''
        logMsg = f'Creating order with params {params}'
        logging.debug(logMsg)
        url = self.endpoint + "user/orders"
        headers = self._getHeaders('POST',
                                   urllib.parse.urlparse(url).path, params)
        response = requests.post(url, headers=headers, data=params)
        return responseParser(response)

    def _cancelOrder(self, id):
        '''Cancels order.

        Example output:
            [{"data": {
                            "id": "a7ed361d-bf39-4b70-a456-e18eadc1b494",
                        "market": "BTC-EUR",
                         "price": "7263.0",
                          "size": "0.01",
                   "size_filled": "0.0",
                        "status": "canceled",
                    "order_type": "sell",
                "operation_type": "market_order",
                    "created_at": "2018-02-04T05:07:59.801504Z"
            }]
        '''
        url = self.endpoint + "user/orders/{}".format(id)
#         print(url)
        headers = self._getHeaders('DELETE',
                                   urllib.parse.urlparse(url).path, {})
        response = requests.delete(url, headers=headers)
        try:
            return responseParser(response)
        except ResponseError:
            logging.warn("Failed to cancel order")

    def _getTrades(self, params):
        '''Returns trades.

        market      required
        since_time  optional Returns orders since the given datetime (non-inc)
        start_time  optional Returns orders beginning with the given datetime
        end_time    optional Returns orders ending with the given datetime

        Example output:
                [
                    'id': '9c56310e-c367-4e60-a392-5f2273dd850f',
                    'price': '0.01947', 'size': '0.0485',
                    'market_name': 'LTC-BTC',
                    'order_left_id': 'b777c7b3-c4cc-45d7-86e8-cc7aaec8ec09',
                    'order_right_id': 'b7365eb3-14e2-48a6-bc43-ca9ddd622672',
                    'side': 'buy',
                    'created_at': '2018-03-03T22:39:56.370132Z'}]

        Notes: newest trades are at start of list
               since_time is not inclusive, i.e. returns open interval
        '''
        url = self.endpoint + "user/trades"
        url2 = urllib.parse.urlparse(url).path
        r = requests.Request('GET', url, data=params).prepare()
        url3 = url2 + '?' + r.body
        headers = self._getHeaders('GET', url3)
        response = requests.get(url+'?'+r.body, headers=headers)
        return responseParser(response)

    def _getBalances(self):
        '''Gets account balances.

        Example output:

        '''
        url = self.endpoint + "user/accounts"
        headers = self._getHeaders('GET', urllib.parse.urlparse(url).path)
        response = requests.get(url, headers=headers)
        jsonResponse = responseParser(response)
        return cfParsers.balance(jsonResponse)

    def _getOrders(self, params):
        '''Gets orders.

        all params are optional
        market      Get only orders from  market e.g. BTC-EUR
        status      [pending, open, partially_filled, fulfilled, all]
        since_time  Returns orders since the given datetime
        start_time  Returns orders beginning with the given datetime
        end_time    Returns orders ending with the given datetime

        Example output:
            [{
                'id': 'b777c7b3-c4cc-45d7-86e8-cc7aaec8ec09',
                'market': 'LTC-BTC',
                'price': '0.01947',
                'size': '0.0485',
                'size_filled': '0.0485',
                'status': 'fulfilled',
                'order_type': 'buy',
                'operation_type': 'market_order',
                'created_at': '2018-03-03T22:39:56.270887Z'
            }]

        If Market doesn't exist, returns all orders
        '''
        url = self.endpoint + "user/orders"
        url2 = urllib.parse.urlparse(url).path
        r = requests.Request('GET', url, data=params).prepare()
        url3 = url2 + '?' + r.body
        headers = self._getHeaders('GET', url3)
        response = requests.get(url+'?'+r.body, headers=headers)
        return responseParser(response)

    def getOpenOrders(self, market):
        jsonOpen = self._getOrders({'status': 'open', 'market': market})
        jsonPartiallyFilled = self.client._getOrders(
                              {'status': 'partially_filled', 'market': market})
        return cfParsers.orders(jsonOpen+jsonPartiallyFilled)

    def createOrder(self, market, side, price, amount):
        '''Creates order.'''
        params = {}
        params['order_type'] = side
        params['size'] = str(amount)
        params['price'] = str(price)
        params['market'] = market
        params['operation_type'] = 'limit_order'
        try:
            return self._createOrder(params)
        except ResponseError:
            raise

    def cancelOrder(self, id):
        try:
            return self._cancelOrder(id)
        except ResponseError:
            raise

    def persistentGetOpenOrders(self, retries):
        '''
        '''
        for i in range(retries):
            try:
                return self._getOrders({'status': 'open'})
            except RateLimitExceededError:
                time.sleep(1)

    def persistentGetTrades(self, market, since, retries):
        '''
        '''
        for i in range(retries):
            try:
                return self._getTrades({'market': market, 'since': since})
            except RateLimitExceededError:
                time.sleep(1)

    def persistentCreateOrder(self, market, side, price, amount, retries):
        '''
        '''
        for i in range(retries):
            try:
                return self.createOrder(market, side, price, amount)
            except RateLimitExceededError:
                time.sleep(1)

    def persistentCancelOrder(self, id, retries):
        '''
        '''
        for i in range(retries):
            try:
                return self.cancelOrder(id)
            except RateLimitExceededError:
                time.sleep(1)
