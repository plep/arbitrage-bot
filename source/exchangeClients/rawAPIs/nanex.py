from selenium import webdriver
from selenium.webdriver.common.by import By
import pyotp
import time
import json
import logging
import deathbycaptcha


class rawNanexClient():
    def __init__(self, market,
                 username='',
                 password='',
                 twoFA=''):

        self.config_file = "driver_config.json"
        self._loadConfig()
        self.market = market[:3]+'NANO'

        self.twoFA = twoFA
        self.username = username
        self.password = password

        self.browser = self.makeBrowser()
        self.balanceBrowser = self.makeBrowser()
        self.fillBrowser = self.makeBrowser()

        self.login([self.browser, self.balanceBrowser, self.fillBrowser])
        # self.login([self.browser])

    def login(self, browsers):
        "logs in the browsers and navigates to the correct pages"
        LOGIN_URL = 'https://nanex.co/auth/login'
        for browser in browsers:
            browser.get(LOGIN_URL)

        time.sleep(1)
        # for browser in browsers:
        #     self.getTokenAndSetInnerHTML(browser)

        decision = -1
        while True:
            decision = input("Enter browser #=1,2,3 to attempt login. Type done if done: ")
            if decision == 'done':
                break
            else:
                self.attemptLogin(browsers[int(decision)-1])

        try:
            self.refreshBrowsers()
        except Exception as e:
            input("Failed to click buttons, are you sure you're logged in?")
            self.login([self.browser, self.balanceBrowser, self.fillBrowser])

    def attemptLogin(self, browser):
        self.enter_login_info(browser)
        time.sleep(0.5)
        self.click_login_and_enter_2fa(browser)
        time.sleep(0.5)

    def enter_login_info(self, browser):
        emailField = browser.find_element_by_id('loginEmail')
        passwordField = browser.find_element_by_id('loginPassword')
        emailField.clear()
        emailField.send_keys(self.username)
        passwordField.clear()
        passwordField.send_keys(self.password)

    def click_login_and_enter_2fa(self, browser):
        LOGIN_BUTTON = '//*[@id="login"]/div[3]/div[2]/button'
        loginButton = browser.find_element_by_xpath(LOGIN_BUTTON)
        loginButton.click()
        time.sleep(4)
        code = pyotp.TOTP(self.twoFA).now()
        browser.switch_to.active_element.send_keys(code)

    def getTokenAndSetInnerHTML(self, browser):
        token = None
        while token is None:
            token = self.getCaptchaToken()
            print(f"Token = {token}")

        browser.execute_script(f"document.getElementById('g-recaptcha-response-1').innerHTML = '{token}'  ")

    def getCaptchaToken(self, keyFile="keys.json"):
        logging.info('Getting captcha token...')
        NANEX_SITE_KEY = "6Ld5TD8UAAAAALGQTCTxZYxxq0hS5p1Ei3GlrZwv"
        NANEX_URL = "https://nanex.co/auth/login"

        siteKey = NANEX_SITE_KEY
        siteUrl = NANEX_URL
        keys = json.load(open(keyFile))[-1]
        username = keys['username']
        password = keys['password']
    # Put the proxy and reCaptcha token data
        Captcha_dict = {
            'proxytype': 'HTTP',
            'googlekey': siteKey,
            'pageurl': siteUrl}

        json_Captcha = json.dumps(Captcha_dict)

        client = deathbycaptcha.HttpClient(username, password)

        try:
            # balance = client.get_balance()
            # Put your CAPTCHA type and Json payload here:
            captcha = client.decode(type=4, token_params=json_Captcha)
            if captcha:
                    # The CAPTCHA was solved; captcha["captcha"] item holds its
                # numeric ID, and captcha["text"] item its a text token".
                logging.info('Captcha received')
                return captcha["text"]

                if '':  # check if the CAPTCHA was incorrectly solved
                    client.report(captcha["captcha"])
        except deathbycaptcha.AccessDeniedException:
            # Access to DBC API denied, check your credentials and/or balance
            print("error: Access to DBC API denied, check your cred or balance")

    def _loadConfig(self):
        config = json.load(open(self.config_file))
        self.DRIVERPATH = config['DRIVERPATH']
        self.CHROMEPATH = config['CHROMEPATH']

    def refreshBrowsers(self):
        marketUrl = 'https://nanex.co/exchange/'+self.market
        balanceUrl = 'https://nanex.co/wallet/NANO'

        self.browser.get(marketUrl)
        self.fillBrowser.get(marketUrl)
        self.balanceBrowser.get(balanceUrl)

        time.sleep(2)
        FILL_BUTTON = "/html/body/app-root/main/app-exchange/div[2]/div[2]/div[1]/div[2]/div/ul/li[2]/a"
        fillButton = self.fillBrowser.find_element_by_xpath(FILL_BUTTON)
        fillButton.click()

        SELL_BUTTON = "/html/body/app-root/main/app-exchange/div[2]/div[2]/div[2]/div[2]/orderform/div/div/div[1]/form/div[1]/a[2]"
        sellButton = self.fillBrowser.find_element_by_xpath(SELL_BUTTON)
        sellButton.click()

        ORDER_BUTTON = "/html/body/app-root/main/app-exchange/div[2]/div[2]/div[1]/div[2]/div/ul/li[1]/a"
        orderButton = self.browser.find_element_by_xpath(ORDER_BUTTON)
        orderButton.click()

        BUY_BUTTON = "/html/body/app-root/main/app-exchange/div[2]/div[2]/div[2]/div[2]/orderform/div/div/div[1]/form/div[1]/a[1]"
        buyButton = self.browser.find_element_by_xpath(BUY_BUTTON)
        buyButton.click()

    def makeBrowser(self):
        options = webdriver.ChromeOptions()
        options.add_argument("--window-size=1920x800")
        options.add_argument('--disable-extensions')
        # options.add_argument('--headless')
        options.add_argument("--start-maximized")
        options.add_argument('--disable-gpu')
        options.add_argument('--no-sandbox')
        options.binary_location = self.CHROMEPATH
        driver = webdriver.Chrome(chrome_options=options,
                                executable_path=self.DRIVERPATH)  # noqa
        return driver

    def authenticateBrowser(self, browser):
        token = self.getCaptchaToken()


    def getBooks(self):
        bookContainer = self.browser.find_element_by_xpath(
                        '//*[@id="orderbook-scroller"]/table/tbody')
        return self.getBookFromText(bookContainer.text.split('\n'))

    def getBookFromText(self, bookRows):
        sellBook = []
        buyBook = []
        sellsDone = False
        for row in bookRows:
            cols = row.split(' ')
            if len(cols) == 3 and not sellsDone:
                sellBook.append([float(ele) for ele in cols])
            elif len(cols) == 3 and sellsDone:
                buyBook.append([float(ele) for ele in cols])
            elif len(cols) != 3:
                sellsDone = True
        sellBook.reverse()
        return {'sell': sellBook, 'buy': buyBook}

    def getBalances2(self):
        # unfortunately doesn't get reserved balances
        btcPath = "/html/body/app-root/main/app-exchange/div[2]/div[2]/div[2]/div[2]/orderform/div/table/tbody/tr[1]/td[2]"
        btc = float(self.browser.find_element_by_xpath(btcPath).text.split(' ')[0])
        xrbPath = "/html/body/app-root/main/app-exchange/div[2]/div[2]/div[2]/div[2]/orderform/div/table/tbody/tr[2]/td[2]"
        xrb = float(self.browser.find_element_by_xpath(xrbPath).text.split(' ')[0])
        return {'XRB': xrb, "BTC": btc}

    def getBalances(self):
        walletText = self.balanceBrowser.find_element(By.XPATH,
                "/html/body/app-root/main/app-wallet/div/div/div[1]/ul"
                    ).text  # noqa
        return self.parseWalletText(walletText)

    def parseWalletText(self, walletText):
        walletText = walletText.split('\n')
        coinNames = [row.split(' ')[1][1:-1] for row in walletText[::3]]
        freeBalances = [float(row.split(' ')[1])
                        for row in walletText[1::3]]
        reservedBalances = [float(row.split(' ')[2])
                            for row in walletText[2::3]]
        coinDatas = [{'free': free, 'total': free + reserved} for
            (free, reserved) in zip(freeBalances, reservedBalances)]  # noqa
        balances = dict(zip(coinNames, coinDatas))
        balances['XRB'] = balances['NANO']
        return balances

    def getFills(self):
        fillText = self.fillBrowser.find_element_by_id('fills').text.split('\n')[1:]
        fills = [self.parseFillText(fillRow) for fillRow in fillText]
        return fills

    def parseFillText(self, fillRow):
        fillSplit = fillRow.split(' ')
        fillData = {}
        side = fillSplit[0]
        if side == 'Buy':
            fillData['side'] = 'buy'
        elif side == 'Sell':
            fillData['side'] = 'sell'
        else:
            raise Exception('No side!')
        fillData['market'] = self.market
        fillData['amount'] = fillSplit[1]
        fillData['volume'] = fillSplit[2]
        fillData['price'] = fillSplit[3]
        fillData['fee'] = fillSplit[4]
        fillData['timestamp'] = fillSplit[6] + fillSplit[7]
        fillHash = fillData['amount'][:3]+fillData['price'][:6]+fillData['volume'][:6]+fillData['timestamp']
        fillData['id'] = fillHash
        return fillData

    def getOrders(self):
        orderContainer = self.browser.find_element_by_id('orders')
        orderRows = orderContainer.find_elements_by_tag_name('tr')[1:]
        orders = [self.parseOrderRow(orderRow) for orderRow in orderRows]
        return orders

    def parseOrderRow(self, orderRow):
        orderData = {}
        orderData['cancelButton'] = orderRow.find_elements_by_tag_name('td')[-1]
        cols = orderRow.text.split(' ')
        orderData['timestamp'] = cols[0]+cols[1]+cols[2]
        orderData['price'] = cols[5]
        orderData['filledAmount'] = cols[6]
        orderData['totalAmount'] = cols[7]
        orderData['volume'] = cols[8]
        side = cols[4]
        if side == 'Buy':
            orderData['side'] = 'buy'
        elif side == 'Sell':
            orderData['side'] = 'sell'
        return orderData

    def placeBuy(self, price, amount):
        priceField = self.browser.find_element_by_id('limit_price')
        amountField = self.browser.find_element_by_id('limit_amount')
        priceField.clear()
        amountField.clear()
        priceField.send_keys(price)
        amountField.send_keys(amount)

        PLACE_BUTTON = "/html/body/app-root/main/app-exchange/div[2]/div[2]/div[2]/div[2]/orderform/div/div/div[1]/form/a/span"
        placeButton = self.browser.find_element_by_xpath(PLACE_BUTTON)
        placeButton.click()
        placeButton.click()
        return {'id': 'No ID for NANEX Exchange '}

    def placeSell(self, price, amount):
        priceField = self.fillBrowser.find_element_by_id('limit_price')
        amountField = self.fillBrowser.find_element_by_id('limit_amount')
        priceField.clear()
        amountField.clear()
        priceField.send_keys(price)
        amountField.send_keys(amount)

        PLACE_BUTTON = "/html/body/app-root/main/app-exchange/div[2]/div[2]/div[2]/div[2]/orderform/div/div/div[1]/form/a/span"
        placeButton = self.fillBrowser.find_element_by_xpath(PLACE_BUTTON)
        placeButton.click()
        placeButton.click()
        return {'id': 'No ID for NANEX Exchange '}

    def cancelSideOrders(self, side):
        orders = self.getOrders()
        for order in orders:
            if order['side'] == side:
                order['cancelButton'].click()
        return None

    def tearDown(self):
        self.browser.quit()
        self.balanceBrowser.quit()
