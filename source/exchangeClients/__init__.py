from .myBigone import BigoneClient
from .myCcxt import CcxtClient
from .myCcxt import BtcMarketsClient
from .myCoinfalcon import cfClient
from .myQuadriga import QuadrigaClient
from .myKucoin import KucoinClient
from .myNanex import NanexClient
from .myMercatox import MercatoxClient