class QuadrigaClient():

    def __init__(self, rawClient, market):
        self.rawClient = rawClient
        self.lastUpdate = None
        self.market = market
        self.sMarket = self.market[:3].lower()+'_'+self.market[-3:].lower()
        self.rawBook = self.rawClient.book(self.sMarket)
        self.AAA = self.sMarket[:3]
        self.BBB = self.sMarket[-3:]

        self.seenTradeIds = []

        # Load up our past trades
        self.getNewTrades()

    def getOrderBooks(self):
        rawResponse = self.rawBook.get_public_orders()
        buyBook = [[float(value) for value in entry]
                   for entry in rawResponse['bids']]
        sellBook = [[float(value) for value in entry]
                    for entry in rawResponse['asks']]
        return {'buy': buyBook, 'sell': sellBook}

    def getBalances(self):
        '''Input is dictionary from quadrigaClient.get_balance()'''
        rawResponse = self.rawClient.get_balance()
        balances = {}
        for key, value in rawResponse.items():
            currencyName = key[:3].upper()
            balanceType = key[4:]
            if balanceType == 'available':
                balances[currencyName] = {'free': float(value)}
        for key, value in rawResponse.items():
            currencyName = key[:3].upper()
            balanceType = key[4:]
            if balanceType == 'reserved':
                balances[currencyName]['total'] = (
                            balances[currencyName]['free'] + float(value))
        return balances

    def getOpenOrders(self):
        rawResponse = self.rawBook.get_user_orders()
        orders = []
        for entry in rawResponse:
            order = {}
            order['id'] = entry['id']
            order['market'] = self.market
            if entry['type'] == 0:
                order['side'] = 'buy'
            elif entry['type'] == 1:
                order['side'] = 'sell'
            order['price'] = float(entry['price'])
            order['amount'] = float(entry['amount'])
            orders.append(order)
        return orders

    def getNewTrades(self):
        newTrades = []
        # only want trades (type = 2)
        rawResponse = [entry for entry in
                       self.rawBook.get_user_trades(limit=20)
                       if entry['type'] == 2]
        for entry in rawResponse:
            id = entry['id']
            if id not in self.seenTradeIds:
                self.seenTradeIds.append(id)
                trade = self.interpretTrade(entry)
                newTrades.append(trade)
        return newTrades

    def interpretTrade(self, entry):
        '''quadriga api has wierd format for trades'''
        trade = {}
        trade['id'] = entry['id']
        trade['market'] = self.market
        trade['price'] = float(entry['rate'])
        trade['amount'] = abs(float(entry[self.AAA]))
        if float(entry[self.BBB]) < 0:
            trade['side'] = 'buy'
        else:
            trade['side'] = 'sell'
        trade['timestamp'] = entry['datetime']
        trade['fee'] = float(entry['fee'])
        return trade

    def placeOrder(self, order):
        if order.side == 'sell':
            return self.rawBook.sell_limit_order(
                             order.amount, order.price)
        elif order.side == 'buy':
            return self.rawBook.buy_limit_order(
                             order.amount, order.price)
        else:
            raise TypeError(f'Order must buy or sell, got {order.side}')

    def cancelOrder(self, order):
        return self.rawClient.cancel_order(order.id)
