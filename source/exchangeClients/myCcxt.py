# Parsing functions for raw json coinfalon API responses
import logging
from ccxt.base.errors import ExchangeError
import json
import exceptions


class CcxtClient:

    def __init__(self, rawClient, market):
        '''client must be a ccxt client '''
        self.rawClient = rawClient
        self.lastUpdate = None
        self.market = market

    def sMarket(self):
        return self.market[:3]+'/'+self.market[-3:]

    def dMarket(self):
        return self.market[:3]+'-'+self.market[-3:]

    def getBalances(self):
        '''Input is dictionary from coinFalcon.getBalance'''
        jsonResponse = self.rawClient.fetchBalance()
        frees = jsonResponse['free']
        totals = jsonResponse['total']
        balances = {}
        for currency, value in frees.items():
            data = {}
            data['free'] = value
            data['total'] = totals[currency]
            balances[currency] = data
        return balances

    def getOpenOrders(self):
        jsonOpen = self.rawClient.fetchOpenOrders(self.sMarket())
        orders = []
        for entry in jsonOpen:
            order = {}
            order['id'] = entry['id']
            order['market'] = self.market
            order['side'] = entry['side']
            order['price'] = entry['price']
            order['amount'] = entry['remaining']
            orders.append(order)
        return orders

    def getOrderBooks(self):
        jsonBook = self.rawClient.fetchOrderBook(self.sMarket())
        sellBook = jsonBook['asks']
        buyBook = jsonBook['bids']
        bookPair = {'buy': buyBook, 'sell': sellBook}
        return bookPair

    def placeOrder(self, order):
        if order.side == 'sell':
            return self.rawClient.createLimitSellOrder(
                            self.sMarket(), order.amount, order.price)
        elif order.side == 'buy':
            return self.rawClient.createLimitBuyOrder(
                            self.sMarket(), order.amount, order.price)
        else:
            raise TypeError(f'Order must buy or sell, got {order.side}')

    def cancelOrder(self, order):
        return self.rawClient.cancelOrder(order.id)


class BtcMarketsClient(CcxtClient):
    def getNewTrades(self):
        logging.debug(f'Getting new Trades. Lastupdate: {self.lastUpdate}')
        if self.lastUpdate is None:
            jsonTrades = self.rawClient.fetchMyTrades(self.sMarket())
        else:
            jsonTrades = self.rawClient.fetchMyTrades(
                                self.sMarket(), since=self.lastUpdate)
        trades = []
        for entry in jsonTrades:
            trade = {}
            trade['id'] = entry['id']
            trade['market'] = self.market
            trade['price'] = entry['price']
            trade['amount'] = entry['amount']
            trade['side'] = entry['side']
            trade['timestamp'] = entry['timestamp']
            trades.append(trade)
        if trades:
            self.lastUpdate = int(trades[0]['id'])+1
        return trades

    def placeOrder(self, order):
        try:
            if order.side == 'sell':
                return self.rawClient.createLimitSellOrder(
                                self.sMarket(), order.amount, order.price)
            elif order.side == 'buy':
                return self.rawClient.createLimitBuyOrder(
                                self.sMarket(), order.amount, order.price)
            else:
                raise TypeError(f'Order must buy or sell, got {order.side}')
        except ExchangeError as e:
            errorData = json.loads(str(e).split(' ', 1)[1])
            if errorData['errorCode'] == 3:
                raise exceptions.InsufficientBalanceError(str(e))
            else:
                raise e
