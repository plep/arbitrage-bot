import time
import exceptions
import bigone


class BigoneClient():

    def __init__(self, rawClient, market):
        self.rawClient = rawClient
        self.lastUpdate = None
        self.market = market
        self.AAA = self.market[:3]
        self.BBB = self.market[-3:]

        self.seenTradeIds = []
        self.lastCall = time.time()
        self.delay = 0  # 1 second between each API call
        self.seenTradeIds = []
        # Load up our past trades
        self.getNewTrades

        def monkey_get_orders(self, symbol):
            return self._get(f'orders?market={symbol}', True)
        self.rawClient.get_orders = monkey_get_orders

    def getOrderBooks(self):
        return self.slowRequest(self._getOrderBooks)

    def getBalances(self):
        return self.slowRequest(self._getBalances)

    def getOpenOrders(self):
        return self.slowRequest(self._getOpenOrders)

    def getNewTrades(self):
        return self.slowRequest(self._getNewTrades)

    def placeOrder(self, order):
        return self.slowRequest(self._placeOrder, order)

    def cancelOrder(self, order):
        return self.slowRequest(self._cancelOrder, order)

    def _getOrderBooks(self):
        rawResponse = self.rawClient.get_order_book(self.market)
        buyBook = [[float(entry['price']), float(entry['amount'])]
                   for entry in rawResponse['bids']]
        sellBook = [[float(entry['price']), float(entry['amount'])]
                    for entry in rawResponse['asks']]
        return {'buy': buyBook, 'sell': sellBook}

    def _getBalances(self):
        '''Input is dictionary from quadrigaClient.get_balance()'''
        rawResponse = self.rawClient.get_accounts()
        balances = {}
        for entry in rawResponse:
            currencyName = entry['account_type']
            data = {}
            data['reserved'] = float(entry['frozen_balance'])
            data['free'] = float(entry['active_balance'])
            data['total'] = data['reserved']+data['free']
            balances[currencyName] = data
        return balances

    def _getOpenOrders(self):
        orders = []
        rawResponse = self.rawClient.get_orders(self.rawClient,
                                                self.market)
        for entry in rawResponse:
            order = {}
            order['id'] = entry['order_id']
            order['price'] = float(entry['price'])
            order['market'] = self.market
            order['amount'] = (float(entry['amount']) -
                               float(entry['filled_amount']))
            if entry['order_side'] == 'BID':
                order['side'] = 'buy'
            if entry['order_side'] == 'ASK':
                order['side'] = 'sell'
            order['timestamp'] = entry['created_at']
            orders.append(order)
        return orders

    def _getNewTrades(self):
        '''Gets trades since self.lastUpdate and update self.lastUpdate '''
        newTrades = []
        rawResponse = self.rawClient.get_trades(
                        symbol=self.market, limit=10)
        for entry in rawResponse:
            id = entry['trade_id']
            if id not in self.seenTradeIds:
                self.seenTradeIds.append(id)
                trade = {}
                trade['id'] = entry['trade_id']
                trade['market'] = self.market
                trade['price'] = float(entry['price'])
                trade['amount'] = float(entry['amount'])
                trade['timestamp'] = entry['created_at']
                if entry['trade_side'] == 'BID':
                    trade['side'] = 'buy'
                elif entry['trade_side'] == 'ASK':
                    trade['side'] = 'sell'
                newTrades.append(trade)
        return newTrades

    def _placeOrder(self, order):
        try:
            if order.side == 'sell':
                data = self.rawClient.create_order(self.market,
                                                   'ASK',
                                                   str(order.price),
                                                   str(order.amount))
            elif order.side == 'buy':
                data = self.rawClient.create_order(self.market,
                                                   'BID',
                                                   str(order.price),
                                                   str(order.amount))
            else:
                raise TypeError(f'Order must buy or sell, got {order.side}')

        except bigone.exceptions.BigoneAPIException as ee:
            errorCode = str(ee).split(' ', 3)[1]
            if errorCode == '20105:':
                raise exceptions.InsufficientBalanceError(str(ee))
            elif errorCode == '20109:':
                raise exceptions.OrderTooSmallError(str(ee))
            else:
                raise ee
        else:
            return {'id': data['order_id']}

    def _cancelOrder(self, order):
        try:
            self.rawClient.cancel_order(order.id)
        except bigone.exceptions.BigoneAPIException as ee:
            errorCode = str(ee).split(' ', 3)[1]
            if errorCode == '20108:':
                raise exceptions.OrderDoesNotExistError(str(ee))

    def slowRequest(self, func, order=None):
        timeTillNextReq = self.lastCall + self.delay - time.time()
        if timeTillNextReq > 0:
            time.sleep(timeTillNextReq)
        self.lastCall = time.time()
        if order is None:
            return func()
        else:
            return func(order)
