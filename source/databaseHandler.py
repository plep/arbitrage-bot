import peewee as pw
import datetime
import time

db = pw.SqliteDatabase('ui_data.db')
db.connect()
coin_list = ['BTC', 'XRB', 'ETH', 'LTC', 'VEN', 'REQ', 'LINK']


def save_balance_info(raw_balance_info):
    '''Pass in a dictionary with the info'''
    rbi = raw_balance_info
    now = datetime.datetime.now()
    balance_info = ExchangeBalanceInfo.create(name=rbi['name'],
                                              BTC=rbi['BTC'],
                                              XRB=rbi['XRB'],
                                              ETH=rbi['ETH'],
                                              LTC=rbi['LTC'],
                                              timestamp=now
                                              )
    balance_info.save()


def get_and_save_mc_balances(marketController):
    raw_balance_info = {'name': marketController.configName}
    for coin in coin_list:
        try:
            balance = marketController.balances[coin]['total']
        except KeyError:
            balance = 0
        raw_balance_info[coin] = balance
    save_balance_info(raw_balance_info)


def get_and_save_bex_balances(bex):
    raw_balance_info = {'name': 'binance'}
    for coin in coin_list:
        try:
            balance = bex.balances['total'][coin]
        except KeyError:
            balance = 0
        raw_balance_info[coin] = balance
    save_balance_info(raw_balance_info)


class BaseModel(pw.Model):
    class Meta:
        database = db


class ExchangeBalanceInfo(BaseModel):
    name = pw.CharField()
    BTC = pw.FloatField()
    ETH = pw.FloatField()
    LTC = pw.FloatField()
    XRB = pw.FloatField()
    timestamp = pw.TimestampField()


db.create_tables([ExchangeBalanceInfo])
