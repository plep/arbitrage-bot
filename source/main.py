from marketController import MarketController
from binance import BinanceExchange
import sys
import mainLoop
import time
import logging
from strategy import Strategy


# Setting up logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
logFormatter = logging.Formatter("%(asctime)s %(message)s")
rootLogger = logging.getLogger()

fileHandler = logging.FileHandler("{0}".format("output.log"))
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

# Create the main objects
keyFile = 'keys.json'
configFile = sys.argv[1]


marketController = MarketController(configFile, keyFile)
bex = BinanceExchange(configFile, keyFile)
strategy = Strategy(configFile)
time.sleep(0.5)

ml = mainLoop.MainLoop(marketController, bex, strategy, configFile)


# Start to bot
logging.info(f'Starting bot')

if sys.argv[2] == 'run':
    ml.loop()
else:
    print('Testing mode')


# Tons of stuff to do:
# Figure out how to use open orders
# Figure out why quoting engine is fucked
#  figure out how to elegantly encapsualte thte fact we need two markets
# seperation of exchange1 and exchange 2
# For nanex: minimize the amount of clicking.
# need 3 browser windows??
# maxAmount seems too big. I don't have enough nano on binance to cover.
