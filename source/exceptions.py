class InsufficientBalanceError(Exception):
    def __init__(self, message):
        self.message = message


class OrderTooSmallError(Exception):
    def __init__(self, message):
        self.message = message


class OrderDoesNotExistError(Exception):
    def __init__(self, message):
        self.message = message
