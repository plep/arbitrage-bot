import coinFalcon
import exchangeStateAsync
import logging
import util

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')

keyDict = util.loadKeys('keys.json')
key = keyDict['coinfalcon']['key']
secret = keyDict['coinfalcon']['secret']
cPub = coinFalcon.coinFalcon_public()
cPrivate = coinFalcon.coinFalcon_private(key, secret)


ex = exchangeStateAsync.Exchange(cPub, cPrivate,
                                 ['LTC-BTC', 'ETH-BTC', 'XRB-BTC'])
