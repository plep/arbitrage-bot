import coinFalcon
import logging
import json

keyDict = json.load(open('keys.json'))
key = keyDict['key']
secret = keyDict['secret']

cPub = coinFalcon.coinFalcon_public()
cPrivate = coinFalcon.coinFalcon_private(key, secret)

# print(cPub.getBook('LTC-BTC', 1))
# print("Balances: ")
# print(cPrivate._getBalances())
# print("Orders: ")
# print(cPrivate._getOrders({'status': 'fulfilled'}))
# print("Trades: ")
# print(cPrivate._getTrades({'market': "LTC-BTC"}))
# print("Trades Since : ")
# print(cPrivate._getTrades({'market': "LTC-BTC",
#                           'since_time': '2018-03-03T22:38:56.370132Z'}))
if __name__ == '__main__':
    logging.basicConfig(filename='tests.log',
                        level=logging.INFO, format='%(asctime)s %(message)s')
    print(cPrivate.createOrder('LTC-BTC', 'sell', 1, 0.01))
    print(cPrivate.cancelOrder('234fds'))
    print(cPrivate.cancelOrder('4212627b-8d57-47e9-b75b-69aeba290e53'))
