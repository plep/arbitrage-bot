import json
import logging
from exchangeStateAsync import ExchangeController
from marketInfo import MarketInfo
import coinFalcon
import util
from binance import BinanceExchange
import ccxt
import mainLoop
import time


def loadConfig(configFile='config.json'):
    config = json.load(open(configFile))
    keys = util.loadKeys()
    exchangeControllers = []
    for exchangeName, marketList in config['exchanges'].items():
        client = makeClient(exchangeName, keys[exchangeName])
        exchangeController = ExchangeController(exchangeName, client)
        exchangeControllers.append(exchangeController)
        for marketParams in marketList:
            marketInfo = MarketInfo(marketParams['market'])
            exchangeController.marketInfos[marketParams['market']] = marketInfo
            marketInfo.setParams(marketParams)
        exchangeController._generateCoinsToMarkets()
    return exchangeControllers


def makeClient(exchange, exchangeKeys):
    if exchange == 'coinfalcon':
        key = exchangeKeys['key']
        secret = exchangeKeys['secret']
        client = coinFalcon.Client(key, secret)
        return client
    if exchange == 'binance':
        key = exchangeKeys['key']
        secret = exchangeKeys['secret']
        return ccxt.binance({'apiKey': key, 'secret': secret})
    else:
        raise Exception("Exchange not recognized!")


logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
logFormatter = logging.Formatter("%(asctime)s %(message)s")
rootLogger = logging.getLogger()

fileHandler = logging.FileHandler("{0}".format("output.log"))
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

exchangeControllers = loadConfig()

markets = set([])
for controller in exchangeControllers:
    for market in controller.markets:
        markets.add(market)
markets = list(markets)


biClient = makeClient('binance', util.loadKeys()['binance'])

bex = BinanceExchange(biClient, markets)
ml = mainLoop.MainLoop(exchangeControllers, bex)
ml.startBot()
bex.updateState()
