import mainLoop
import util
from binance import BinanceExchange
import ccxt
import exchangeStateAsync
import coinFalcon
import logging
import time

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
logFormatter = logging.Formatter("%(asctime)s %(message)s")
rootLogger = logging.getLogger()

fileHandler = logging.FileHandler("{0}".format("output.log"))
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

# consoleHandler = logging.StreamHandler()
# consoleHandler.setFormatter(logFormatter)
# rootLogger.addHandler(consoleHandler)

# Create raw API clients
keyDict = util.loadKeys('keys.json')
key = keyDict['coinfalcon']['key']
secret = keyDict['coinfalcon']['secret']
cPub = coinFalcon.coinFalcon_public()
cPrivate = coinFalcon.coinFalcon_private(key, secret)

markets = ['ETH-BTC']

# Loading Binance
keyDict = util.loadKeys('keys.json')
key = keyDict['binance']['key']
secret = keyDict['binance']['secret']
biClient = ccxt.binance({'apiKey': key, 'secret': secret})

bex = BinanceExchange(biClient, markets)
bex.updateState()


# Create exchange interface object
ex = exchangeStateAsync.Exchange(cPub, cPrivate,
                                 markets)

ml = mainLoop.mainLoop([ex], bex)
ml.startBot()
bex.updateState()
while True:
    time.sleep(2)
    ml.mainLoopStep()
