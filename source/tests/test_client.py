from util import makeClient
import sys
from order import Order


keyFile = 'keyFile.json'
exchangeKeyRef = sys.argv[1]
exchangeAPIRef = sys.argv[1]

client = makeClient(exchangeKeyRef, exchangeAPIRef, keyFile, sys.argv[2])
rawClient = client.rawClient

order = Order(client.market, 'buy')
order.price = 0.02
order.amount = 0.001
order.exchangeName = exchangeAPIRef
# print(client.placeOrder(order))

bigOrder = Order(client.market, 'buy')
bigOrder.price = 0.02
bigOrder.amount = 100
bigOrder.exchangeName = exchangeAPIRef
