import ccxtClient
from util import loadKeys, makeClient
import sys
import ccxt

keyFile = 'keyFile.json'
exchangeKeyRef = sys.argv[1]
exchangeCcxtName = sys.argv[2]
exchangeAPIRef = sys.argv[3]

rawClient = makeClient(exchangeKeyRef, exchangeAPIRef, keyFile)

client = ccxtClient.CcxtClient(rawClient)
client.market = 'LTC-BTC'