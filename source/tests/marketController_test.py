from marketController import MarketController

marketController = MarketController('config.json', None)
marketController.bookPair['buy'] = [[1.5, 1], [1.4, 2], [1.3, 3]]
marketController.bookPair['sell'] = [[1.3, 1], [1.4, 2], [1.5, 3]]


def test(result, expected, testName):
    if abs(result - expected) < 10e-12:
        print(f'{testName} successful!')
    else:
        print(
                f'{testName} failed! Expected {expected}, got {result}')

# Test getTopPrice


marketController.targetDepth = 0

testName = 'getTopPrice: marketDepth = 0'
test(marketController.getTopPrice('sell'), 1.3, testName)
test(marketController.getTopPrice('buy'), 1.5, testName)

marketController.targetDepth = 1.49

testName = 'getTopPrice: marketDepth = 1.49'
test(marketController.getTopPrice('sell'), 1.4, testName)
test(marketController.getTopPrice('buy'), 1.5, testName)

marketController.targetDepth = 1.5

testName = 'getTopPrice: marketDepth = 1.5'
test(marketController.getTopPrice('sell'), 1.4, testName)
test(marketController.getTopPrice('buy'), 1.4, testName)


marketController.targetDepth = 1.51

testName = 'getTopPrice: marketDepth = 1.51'
test(marketController.getTopPrice('sell'), 1.4, testName)
test(marketController.getTopPrice('buy'), 1.4, testName)


marketController.targetDepth = 4.2

testName = 'getTopPrice: marketDepth = 4.2'
test(marketController.getTopPrice('sell'), 1.5, testName)
test(marketController.getTopPrice('buy'), 1.4, testName)


sellOrder = marketController.createPreOrder('sell')
buyOrder = marketController.createPreOrder('buy')
marketController.sellOrder = sellOrder
marketController.buyOrder = buyOrder

# Now with our own order
print("Now with our own order")

sellOrder.price = 1.2
buyOrder.price = 1.7
sellOrder.amount = 2
buyOrder.amount = 2

marketController.bookPair['sell'] = [[1.2, 3], [1.3, 1], [1.4, 2], [1.5, 3]]
marketController.bookPair['buy'] = [[1.7, 3], [1.5, 1], [1.4, 2], [1.3, 3]]

marketController.targetDepth = 1.19

testName = 'getTopPrice: marketDepth = 1.19'
test(marketController.getTopPrice('sell'), 1.2, testName)
test(marketController.getTopPrice('buy'), 1.7, testName)

marketController.targetDepth = 1.21

testName = 'getTopPrice: marketDepth = 1.21'
test(marketController.getTopPrice('sell'), 1.3, testName)
test(marketController.getTopPrice('buy'), 1.7, testName)


marketController.targetDepth = 5

testName = 'getTopPrice: marketDepth = 5'
test(marketController.getTopPrice('sell'), 1.4, testName)
test(marketController.getTopPrice('buy'), 1.4, testName)


# Testing sellOrderDepth
print('Testing sellOrderDepth')
marketController.bookPair['buy'] = [[1.5, 1], [1.4, 2], [1.3, 3]]
marketController.bookPair['sell'] = [[1.3, 1], [1.4, 2], [1.5, 3]]

buyOrder.price = 1.7
buyOrder.amount = 2
sellOrder.price = 1.2
sellOrder.amount = 2


def testSellOrder(sellOrder, expected):
    test(marketController.computeOrderDepth(
        sellOrder)['depth'], expected,  str(sellOrder))


testSellOrder(sellOrder, 0)

sellOrder.price = 1.4
sellOrder.amount = 2
testSellOrder(sellOrder, 1.3)

sellOrder.price = 1.4
sellOrder.amount = 1
testSellOrder(sellOrder, 2.7)

sellOrder.price = 1.3
sellOrder.amount = 1
testSellOrder(sellOrder, 0)


sellOrder.price = 1.3
sellOrder.amount = .8
testSellOrder(sellOrder, .2*1.3)
