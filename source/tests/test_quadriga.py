from util import loadKeys, makeClient

keyFile = 'keyFile.json'
exchangeKeyRef = 'quadrigacx'
exchangeCcxtName = 'quadrigacx'
exchangeAPIRef = 'quadrigacx'

client = makeClient(exchangeKeyRef, exchangeAPIRef, keyFile, 'ETH-BTC')
rawClient = client.rawClient

client.getBalances()