import coinFalcon
import logging
import exchangeStateAsync
import asyncio
import json

keyDict = json.load(open('keys.json'))
key = keyDict['key']
secret = keyDict['secret']

cPub = coinFalcon.coinFalcon_public()
cPrivate = coinFalcon.coinFalcon_private(key, secret)

if __name__ == '__main__':
    logging.basicConfig(
                        level=logging.INFO, format='%(asctime)s %(message)s')
    ex = exchangeStateAsync.exchange(cPub, cPrivate, ['LTC-BTC'])
    ex.updateState()
    print(ex.orders)
