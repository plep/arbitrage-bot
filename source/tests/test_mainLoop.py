import mainLoop
import util
from binance import BinanceExchange
import ccxt
import exchangeStateAsync
import coinFalcon
import logging
from Order import Order

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')

# Create raw API clients
keyDict = util.loadKeys('keys.json')
key = keyDict['coinfalcon']['key']
secret = keyDict['coinfalcon']['secret']
cPub = coinFalcon.coinFalcon_public()
cPrivate = coinFalcon.coinFalcon_private(key, secret)


# Loading Binance
keyDict = util.loadKeys('keys.json')
key = keyDict['binance']['key']
secret = keyDict['binance']['secret']
biClient = ccxt.binance({'apiKey': key, 'secret': secret})

bex = BinanceExchange(biClient, ['LTC-BTC'])
bex.updateState()
liquidityInfo = bex.getLiquidityInfo('LTC-BTC')


# Create exchange interface object
ex = exchangeStateAsync.Exchange(cPub, cPrivate,
                                 ['LTC-BTC'])

ml = mainLoop.mainLoop([ex], bex)

ex.updateState()
exchangeMarketInfo = ex.marketInfos['LTC-BTC']

print(ex.marketInfos['LTC-BTC'].bookPair)

# Test getTopPrice
print("Getting Top Price: ")
print(ml.getTopPrice('sell', ml.exchanges[0].marketInfos['LTC-BTC']))
print(ml.getTopPrice('buy', ml.exchanges[0].marketInfos['LTC-BTC']))


# Test computeOrderDepth
print("Compute Order Depth: ")
book1 = ex.marketInfos['LTC-BTC'].bookPair
order = Order('LTC-BTC', 'buy')
order.price = 0.0192
print(ml.computeOrderDepth(order, book1))
order = Order('LTC-BTC', 'sell')
order.price = 0.022
print(ml.computeOrderDepth(order, book1))


# Test the quoting engine
print("Testing quoting engine: ")
buyOrder, sellOrder = ml.quotingEngine(exchangeMarketInfo, liquidityInfo)
print(buyOrder)
print(sellOrder)

# Test shouldCancelOrder
oldOrder = Order('LTC-BTC', 'buy')
oldOrder.price = 0.01974
oldOrder.pRatio = 0.2
print(ml.shouldCancelOrder(oldOrder, buyOrder, exchangeMarketInfo))
