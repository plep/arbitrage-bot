import util
from binance import BinanceExchange
import ccxt

keyDict = util.loadKeys('keys.json')
key = keyDict['binance']['key']
secret = keyDict['binance']['secret']
biClient = ccxt.binance({'apiKey': key, 'secret': secret})

bex = BinanceExchange(biClient, ['LTC-BTC'])


bex.updateState()

# Test liquidity functions
liquidityFunctions = bex.getLiquidityFunctions('LTC-BTC')
liquidityFunctions['sell'](5)
liquidityFunctions['buy'](5)


# LiquidityInfo object
print("Returning liquidityInfo ")
liquidityInfo = bex.getLiquidityInfo('LTC-BTC')

bex.buyBuffers = {'LTC-BTC': 0.01}