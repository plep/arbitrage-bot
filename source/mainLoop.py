import logging
from util import botMsg
import time
import util
import json
import code
import databaseHandler


class MainLoop:
    def __init__(self, marketController, strongExchange, strategy, configFileName):
        """
        marketcontroller controls the weak exchnage
        strongExchange controls the exchange with better liquidity
        """
        DEFAULT_CONFIG = "../configs/default.json"
        self._loadConfig(DEFAULT_CONFIG)
        self.configFileName = configFileName
        self._loadConfig(self.configFileName)

        self.strongExchange = strongExchange  #
        self.marketController = marketController
        self.strategy = strategy
        self.marketController.updateState()
        self.marketController.processedTrades = []
        self.strongExchange.updateState()
        self.liquidityInfo = self.strongExchange.getLiquidityInfo()

        self.numberOfLoops = 0
        self.last_db_update = time.time()

    def _loadConfig(self, file):
        config = json.load(open(file))["mainLoop"]
        self.interval = config["interval"]
        self.stop = config["stop"]
        self.pause = config["pause"]
        self.interval = config["interval"]

        try:
            self.db_update_frequency = config["db_update_frequency"]
            self.log_strongExchange = config["log_binance"]
        except KeyError:
            self.db_update_frequency = 60  # seconds
            self.log_strongExchange = False

        if self.stop:
            self.expBackOff = 5
            try:
                mc = self.marketController
                bex = self.strongExchange
                strat = self.strategy
                code.interact(local=locals())
            except AttributeError:
                pass  # Don't want to stop until all the stuff has been defined
        if self.pause:
            input("Press Enter to continue")

    def mainLoopStep(self):
        # Make sure configs are up to date
        self._loadConfig(self.configFileName)
        self.marketController._loadConfig(self.configFileName)
        self.strategy._loadConfig(self.configFileName)
        self.strongExchange._loadConfig(self.configFileName)

        # Update our view of the weak exchange
        self.marketController.updateStep()

        # If any of our orders have been filled on the weak exchange,
        # add the corresponding closing action to the queue
        self.updateBuffer()

        # Close the outstanding amount on the strong exchange to realize profits
        self.strongExchange.clearBuffers()

        self.strongExchange.updateState()
        self.liquidityInfo = self.strongExchange.getLiquidityInfo()

        # Recompute the profitability of our open orders using new liquidity info
        self.updateProfitRatios()

        # Place/cancel orders on the weak exchange in line with our strategy
        self.updateActiveOrders(self.strategy)

        self.updateDatabase()

        self.numberOfLoops += 1

    def updateDatabase(self):
        freq = self.db_update_frequency
        if self.numberOfLoops == 0 or time.time() - self.last_db_update > freq:
            logging.info("updating database")
            self.last_db_update = time.time()
            databaseHandler.get_and_save_mc_balances(self.marketController)
            if self.log_strongExchange:
                databaseHandler.get_and_save_bex_balances(self.strongExchange)

    def updateBuffer(self):
        # Look at filled orders on weak exchange and determine
        # what needs to be done on the strong exchange to realize the profits
        if len(self.marketController.processedTrades) > 6:
            botMsg("Too many trades!!!")
            time.sleep(100)

        for trade in self.marketController.processedTrades:
            self.processTrade(trade)
        self.marketController.newTrades = []

    def processTrade(self, trade):
        buyBuffers = self.strongExchange.buyBuffers
        market = trade["market"]
        logging.info(str(trade))
        botMsg(str(trade))
        if trade["side"] == "sell":
            buyBufferChange = trade["amount"]
        elif trade["side"] == "buy":
            buyBufferChange = -trade["amount"]
        try:
            buyBuffers[market] += buyBufferChange
        except KeyError:
            buyBuffers[market] = buyBufferChange

    def updateProfitRatios(self):
        # Recompute the profit ratios based on new liquidity info from strong exchange
        for side, order in self.marketController.trackedOrders.items():
            if order.exists:
                order.update_p_ratio(self.liquidityInfo)

    def updateActiveOrders(self, strategy):
        # Determine what buy/sell orders we should have on the weak exchange
        newOrders = strategy.both_quotes(self.marketController, self.liquidityInfo)

        # Do what it takes for us to have the desired buy/sell orders
        for side, order in self.marketController.trackedOrders.items():
            newOrder = newOrders[side]
            if newOrder.should_cancel:
                self.marketController.cancelOrder(order)
            if newOrder.recommended and not order.exists:
                self.marketController.placeOrder(newOrder)

    def loop(self):
        counter = 0
        self.expBackOff = 5
        MAX_BACKOFF = 1200
        while True:
            time.sleep(self.interval)
            try:
                self.mainLoopStep()
                self.expBackOff = 5
            except Exception as e:
                logging.exception("")
                util.botMsg(self.marketController.configName + ": " + str(e))
                time.sleep(self.expBackOff)
                self.expBackOff = min(2 * self.expBackOff, MAX_BACKOFF)
            if counter % 3 == 1:
                pass
            counter += 1


# rounding issues still messing up the undercut
# need to test potential take

# config settings: takerFee, checkForTakes, PAUSE, STOP

# order class: maker/taker
