import logging
from util import botMsg, makeClient, roundDown
import json


class LiquidityInfo:
    def __init__(self, market):
        self.market = market  # The pair we are trading on
        self.liquidityFunctions = None  # How much it costs to buy/sell a certain amount

        # The nsame of the coins we are trading on
        self.coinAAA = market[:3]
        self.coinBBB = market[-3:]

        self.spotPrices = None

        # Orders that we need to make to close deals and realize profits
        self.buyBuffer = None

    def maxAmount(self, e1OrderCoin, side):
        """Max amount of e1OrderCoin that we can gain/Loss and still close"""
        amtAAA = self.balances[self.coinAAA] + self.buyBuffer

        # Warning: doesn't account for slippage:
        if self.buyBuffer > 0:
            amtBBB = (
                self.balances[self.coinBBB] - self.buyBuffer * self.spotPrices["sell"]
            )
        elif self.buyBuffer <= 0:
            amtBBB = (
                self.balances[self.coinBBB] - self.buyBuffer * self.spotPrices["buy"]
            )

        r = self.btcUseRatio
        if e1OrderCoin == self.coinAAA:
            if side == "buy":  # We need to sell coinAAA
                return r * amtAAA
            if side == "sell":  # We need to buy coinAA
                return r * amtBBB / self.spotPrices["sell"]

        if e1OrderCoin == self.coinBBB:
            if side == "buy":  # we need to buy coinAAA
                return r * amtBBB
            if side == "sell":  # we need to sell coinAAA
                return r * amtAAA * self.spotPrices["buy"]

    def maxClose(self, side):
        """Determines our order size when we need to close a deal"""
        r = self.btcUseRatio
        if side == "buy":
            maxClose = r * self.liquidityFunctions[self.coinBBB](
                self.balances[self.coinBBB]
            )
        if side == "sell":
            maxClose = self.balances[self.coinAAA]

        return max(maxClose, 0)


class BinanceExchange:
    def __init__(self, configFile, keyFile):
        """
        markets are in ETH-BTC form not ETH/BTC form
        """
        # should be read be config
        DEFAULT_CONFIG = "../configs/default.json"
        self.configFile = configFile
        self._loadConfig(DEFAULT_CONFIG)
        self._loadConfig(self.configFile)
        self.client = makeClient("binance", "binance", keyFile, None)
        self.books = {}
        self.balances = {}
        self.buyBuffers = {self.market: 0}  # keys should be markets in / form

    def _loadConfig(self, file):
        config = json.load(open(file))["binance"]
        try:
            self.btcUseRatio = config["btc_use_ratio"]
            self.market = config["market"]
            self.FEE = config["fee"]
            self.marketSigFig = config["market_sig_fig"]
        except KeyError as e:
            logging.info(str(e))

    def updateState(self):
        self._updateBooks()
        self._updateBalances()

    def bMarkForm(self, market):
        return market[:3] + "/" + market[-3:]

    def nMarkForm(self, market):
        return market[:3] + "-" + market[-3:]

    def _clearBuffer(self, market):
        marketSigFig = self.marketSigFig
        minOrderSize = 1 / (10 ** marketSigFig)
        liquidityInfo = self.getLiquidityInfo()
        buyBuffer = self.buyBuffers[market]
        bMarket = self.bMarkForm(market)
        if buyBuffer >= minOrderSize:
            amtAAAorder = min(buyBuffer, liquidityInfo.maxClose("buy"))
            amtAAAorder = roundDown(amtAAAorder, marketSigFig)
            spotPrice = liquidityInfo.spotPrices["buy"]
            if amtAAAorder >= minOrderSize:
                botMsg(f"Buying {amtAAAorder} at price {spotPrice} on {market}...")
                try:
                    self.client.createMarketBuyOrder(bMarket, amtAAAorder)
                except Exception as e:
                    logging.exception("Failed to close buffer")
                    raise e
                else:
                    self.buyBuffers[market] -= amtAAAorder
        elif -buyBuffer >= minOrderSize:
            amtAAAorder = min(-buyBuffer, liquidityInfo.maxClose("sell"))
            amtAAAorder = roundDown(amtAAAorder, marketSigFig)
            spotPrice = liquidityInfo.spotPrices["sell"]
            if amtAAAorder >= minOrderSize:
                botMsg(f"Selling {amtAAAorder} at {spotPrice} on {market}...")
                try:
                    self.client.createMarketSellOrder(bMarket, amtAAAorder)
                except Exception as e:
                    logging.exception("Failed to close buffer")
                    raise e
                else:
                    self.buyBuffers[market] += amtAAAorder

    def clearBuffers(self):
        for market in self.buyBuffers.keys():
            logging.debug("Clearing {market} buffer...")
            self._clearBuffer(market)

    def getLiquidityInfo(self):
        market = self.market
        liquidityInfo = LiquidityInfo(market)
        liquidityInfo.spotPrices = self.getSpotPrices(market)
        liquidityInfo.liquidityFunctions = self.getLiquidityFunctions()
        liquidityInfo.btcUseRatio = self.btcUseRatio
        coinAAA = market[:3]
        coinBBB = market[-3:]
        balances = {}

        # The balances that are available to be used immediately
        balances[coinAAA] = self.balances[coinAAA]["free"]
        balances[coinBBB] = self.balances[coinBBB]["free"]

        liquidityInfo.balances = balances
        liquidityInfo.buyBuffer = self.buyBuffers[market]
        return liquidityInfo

    def getSpotPrices(self, market):
        spotBuy = self.books[market]["buy"][0][0]
        spotSell = self.books[market]["sell"][0][0]
        return {"sell": spotSell, "buy": spotBuy}

    def _updateBooks(self):
        market = self.market
        bMarket = self.bMarkForm(market)
        book = self.client.fetchOrderBook(bMarket)
        self.books[market] = {}
        self.books[market]["buy"] = book["bids"]
        self.books[market]["sell"] = book["asks"]

    def _updateBalances(self):
        self.balances = self.client.fetchBalance()

    def getLiquidityFunctions(self):
        """Returns pair of functions which tell us
        how much it costs to buy/sell a given amount"""
        market = self.market
        book = self.books[market]
        liquidityFunctions = {}
        coinAAA = market[:3]
        coinBBB = market[-3:]
        liquidityFunctions[coinAAA] = self._getSellLiquidity(book["buy"])
        liquidityFunctions[coinBBB] = self._getBuyLiquidity(book["sell"])
        return liquidityFunctions

    def _getSellLiquidity(self, buyBook):
        fee = self.FEE

        def liquidityFunction(amount):
            """Returns how much BBB we can get for given AAA"""
            remainder = amount
            counter = 0
            BBBgain = 0
            while remainder > 0:
                try:
                    currentPrice, currentAmount = buyBook[counter]
                except IndexError:
                    logging.error("Order book not deep enough")
                    raise
                if remainder < currentAmount:
                    BBBgain += remainder * currentPrice
                    remainder = 0
                else:
                    BBBgain += currentAmount * currentPrice
                    remainder -= currentAmount
                counter += 1
            return BBBgain * (1 - fee)  # How much BBB we get

        return liquidityFunction

    def _getBuyLiquidity(self, sellBook):
        fee = self.FEE

        def liquidityFunction(amount):
            """Returns how much AAA we can buy for given BBB"""
            remainder = amount
            counter = 0
            AAAgain = 0
            while remainder > 0:
                try:
                    currentPrice, currentAmount = sellBook[counter]
                except IndexError:
                    logging.error("Order book not deep enough")
                    raise
                currentVolume = currentPrice * currentAmount
                if remainder < currentVolume * (1 + fee):
                    AAAgain += remainder / (currentPrice * (1 + fee))
                    remainder = 0
                else:
                    AAAgain += currentAmount
                    remainder -= currentVolume * (1 + fee)
                counter += 1
            return AAAgain  # How much AAA we get

        return liquidityFunction


# Hacks:
# maxClose, maxAmount, and minOrderSize in clearBuffer