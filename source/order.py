from util import roundUp, roundDown


class Order:
    # Each instance of this class keeps track of all the relevant data
    # for one of our limit orders
    def __init__(self, market, side):
        """
        Attributes:
        market
        side
        amount
        price
        id
        fee
        configName
        - These attributes can change from tick to tick:
        depth
        pRatio
        """
        if side not in ["sell", "buy"]:
            raise TypeError(f"side must be sell or buy, got {side} instead")
        self.market = market
        self.pRatio = 0
        self.side = side
        self.recommended = None
        self.id = None
        self.exists = False  # Whether or not the order actually exists on the exchange
        self.depth = 0
        self.price = 0
        self.amount = 0

    def __str__(self):
        return (
            f"{self.configName}, "
            f"{self.side}, {self.market}, "
            f"Price: {self.price:.5f}, "
            f"Amt: {self.amount:.5f}, "
            f"p: {(100*self.pRatio):.4f}, "
            f"rec: {self.recommended}"
        )

    def __repr__(self):
        return str(self)

    def deactivate(self):
        self.exists = False

    def loss(self):
        """If this order is filled, what will we lose?"""
        market = self.market
        if self.side == "sell":
            return {"amount": self.amount, "coin": market[:3]}
        if self.side == "buy":
            return {"amount": self.volume() * (1 + self.fee), "coin": market[-3:]}

    def gain(self):
        """If this order is filled, what will we gain?"""
        market = self.market
        if self.side == "sell":
            return {"amount": self.volume() * (1 - self.fee), "coin": market[-3:]}
        if self.side == "buy":
            return {"amount": self.amount, "coin": market[:3]}

    def appearsAbove(self, otherPrice):
        """Determines whether this order would appear before or
        after the given price in the order book
        """
        if self.side == "sell":
            return self.price <= otherPrice
        if self.side == "buy":
            return self.price >= otherPrice

    def notOurPart(self):
        return NotOurPart(self)

    def volume(self):
        return self.price * self.amount

    def setPrice(self, price):
        if self.side == "sell":
            self.price = roundDown(price, self.pricePrecision)
        elif self.side == "buy":
            self.price = roundUp(price, self.pricePrecision)

    def setAmount(self, amount):
        self.amount = roundDown(amount, self.amountPrecision)

    def undercut(self, price):
        """Sets our price to be slightly higher/lower than
        given price so that it appears before it in the order book
        """
        if self.side == "sell":
            candidatePrice = roundDown(price, self.pricePrecision)
            if candidatePrice < price:
                self.setPrice(candidatePrice)
            else:
                undercut = 10 ** (-self.pricePrecision)
                self.setPrice(candidatePrice - undercut)
        elif self.side == "buy":
            candidatePrice = roundUp(price, self.pricePrecision)
            if candidatePrice > price:
                self.setPrice(candidatePrice)
            else:
                undercut = 10 ** (-self.pricePrecision)
                self.setPrice(candidatePrice + undercut)

    def update_p_ratio(self, liquidity_info):
        """Returns positive or negative float around 0
        which indicates the percentage profit to be gained if the order
        is hit at this moment
        """
        if self.amount == 0:
            self.amount = 0.001
            changedOrder = True
        else:
            changedOrder = False
        liquidityFunctions = liquidity_info.liquidityFunctions
        liquidityFunction = liquidityFunctions[self.gain()["coin"]]
        exchange2gain = liquidityFunction(self.gain()["amount"])
        orderCost = self.loss()["amount"]
        if changedOrder:
            self.amount == 0
        p_ratio = (exchange2gain / orderCost) - 1
        self.pRatio = p_ratio


class NotOurPart:
    # An object representing the part of an order book entry which
    # does not correspond to one of our orders
    # This is useful because some exchanges will group our orders with other orders
    # of the same price.
    def __init__(self, order):
        self.remaining_amount = order.amount
        self.price = order.price
        self.exists = order.exists

    def __call__(self, price, amount):
        if price == self.price and self.exists:
            amount_due_to_ours = min(self.remaining_amount, amount)
        else:
            amount_due_to_ours = 0
        self.remaining_amount -= amount_due_to_ours
        return price * (amount - amount_due_to_ours)
