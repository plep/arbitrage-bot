import logging
import json


class Strategy():
    
    def __init__(self, configFile):
        DEFAULT_CONFIG = "../configs/default.json"
        self.configFile = configFile
        self._loadConfig(DEFAULT_CONFIG)
        self._loadConfig(self.configFile)

    def _loadConfig(self, file):
        config = json.load(open(file))['strategy']
        try:
            self.target_depths = config['target_depths']
            self.thresholds = config['thresholds']
            self.volume_ratio_cutoff = config['volume_ratio_cutoff']
            self.p_ratio_const = config['p_ratio_const']
        except KeyError as e:
            logging.info(str(e))

    def both_quotes(self, market_controller, liquidity_info):
        ''' This is the main method of this class. 
            It tells us what orders we should have open on the weak exchange,
            in order to profit.
        '''
        return {'buy': self.quote('buy', market_controller, liquidity_info),
                'sell': self.quote('sell', market_controller, liquidity_info)}


    def quote(self, side, market_controller, liquidity_info):
        preOrder = market_controller.createPreOrder(side)
        bestPrice = market_controller.getTopPrice(side,
                                                  self.target_depths[side])
        preOrder.undercut(bestPrice)
        self.calculateAndSetAmount(preOrder, market_controller, liquidity_info)
        self.set_recommendations(preOrder, market_controller)
        logging.info(str(preOrder))
        return preOrder

    def calculateAndSetAmount(self, preOrder, market_controller,
                              liquidity_info, upperBound=100000):
        '''
            Given a preorder with price information filled in,
            Determines the maximum order size we can put in and
            still close on the other side

            Also uses marketController to determine
            whether we should place the or not.
        '''
        e1OrderCoin = market_controller.AAA
        side = preOrder.side

        maxOrder = min(market_controller.maxAmount(side, preOrder.price),
                       liquidity_info.maxAmount(e1OrderCoin, side))
        maxOrder = min(maxOrder, upperBound)
        preOrder.setAmount(market_controller.volumeRatio * maxOrder)
        preOrder.update_p_ratio(liquidity_info)

    def set_recommendations(self, preOrder, market_controller):
        old_order = market_controller.trackedOrders[preOrder.side]
        preOrder.recommended = (preOrder.pRatio > self.thresholds[preOrder.side]
                                and preOrder.amount > preOrder.minOrderSize)
        preOrder.should_cancel = self.shouldCancelOrder(old_order, preOrder)

    def shouldCancelOrder(self, oldOrder, newOrder):
        '''Decides whether to cancel oldOrder

           Returns True or False
        '''
        # Cancel order if it's not profitable anymore,
        # or if the new order is much more profitable,
        # or if the old order is too deep
        targetDepth = self.target_depths[oldOrder.side]
        VOLUMERATIO = self.volume_ratio_cutoff
        PRATIOCONST = self.p_ratio_const
        if not oldOrder.exists:
            return False
        if not newOrder.recommended:
            logging.info(
                f'Cancelling {oldOrder.side} because newOrder not recommended')
            return True
        if newOrder.pRatio > PRATIOCONST * oldOrder.pRatio:
            logging.info(
             f'Cancelling {oldOrder.side} because newOrder has better pRatio')
            return True
        if oldOrder.depth > VOLUMERATIO * targetDepth:
            logging.info(
             f'Cancelling {oldOrder.side} because oldOrder too Deep: '
             f'{oldOrder.depth/oldOrder.price:.5f}')
            return True
        else:
            return False

    def potentialTake(self, side, market_controller, liquidity_info):
        # maker fee, taker feee
        # make/take buy/sell targetDepths
        e1Book = market_controller.bookPair[self.rev(side)]
        preOrder = market_controller.createPreOrder(side)
        preOrder.fee = market_controller.takerFee
        preOrder.price, upperBound = e1Book[0]
        preOrder = self.determineOrderAmount(preOrder, liquidity_info,
                                             upperBound)
        return preOrder
