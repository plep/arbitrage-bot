import json
from math import ceil, floor
import telegram
import logging

# Exchange APIs
import ccxt
import quadriga
import kucoin.client
from bigone.client import Client as rawBigoneClient

import exchangeClients
import exchangeClients.rawAPIs as rawAPIs


def createTelegramBot(file='telegram.json'):
    telegramInfo = json.load(open(file))
    token = telegramInfo['token']
    channel = telegramInfo['channel']
    bot = telegram.Bot(token=token)

    def botMsg(msg):
        try:
            bot.send_message(
                chat_id=channel, text=msg)
            logging.info(msg)
        except Exception:
            logging.exception('Failed to send message')

    return botMsg


def makeClient(exchangeKeyRef, exchangeAPIRef, keyFile, market):
    try:
        keys = loadKeys()[exchangeKeyRef]
        key = keys['key']
        secret = keys['secret']
    except KeyError:
        logging.exception(f"{exchangeKeyRef} not found in {keyFile}!")
        raise
    if exchangeAPIRef == 'coinfalcon':
        rawClient = rawAPIs.rawCoinfalconClient(key, secret)
        client = exchangeClients.cfClient(rawClient, market)
    elif exchangeAPIRef == 'btcmarkets':
        rawClient = ccxt.btcmarkets({'apiKey': key, 'secret': secret})
        client = exchangeClients.BtcMarketsClient(rawClient, market)
    elif exchangeAPIRef == 'binance':
        client = ccxt.binance({'apiKey': key, 'secret': secret})
    elif exchangeAPIRef == 'quadrigacx':
        uid = keys['uid']
        rawClient = quadriga.QuadrigaClient(
                                api_key=key,
                                api_secret=secret,
                                client_id=uid)
        client = exchangeClients.QuadrigaClient(rawClient, market)
    elif exchangeAPIRef == 'kucoin':
        rawClient = kucoin.client.Client(key, secret)
        client = exchangeClients.KucoinClient(rawClient, market)
    elif exchangeAPIRef == 'bigone':
        rawClient = rawBigoneClient(secret)
        client = exchangeClients.BigoneClient(rawClient, market)
    elif exchangeAPIRef == 'nanex':
        twoFA = keys['twoFA']
        rawClient = rawAPIs.rawNanexClient(market,
                                           username=key,
                                           password=secret,
                                           twoFA=twoFA)
        client = exchangeClients.NanexClient(rawClient, market)
    elif exchangeAPIRef == 'mercatox':
        twoFA = keys['twoFA']
        rawClient = rawAPIs.rawMercatoxClient(market,
                                           username=key,
                                           password=secret,
                                           twoFA=twoFA)
        client = exchangeClients.MercatoxClient(rawClient, market)
    else:
        raise Exception(f"{exchangeAPIRef} not supported!")
    return client


def loadKeys(file='keys.json'):
    '''
    Returns dict of {'key': ----, 'secret': ----}
    '''
    listOfKeys = json.load(open(file))
    keys = {}
    for entry in listOfKeys:
        exchange = entry.pop('exchange')
        keys[exchange] = entry
    return keys


def roundUp(float, digits):
    return ceil(float*10**digits)/10**digits


def roundDown(float, digits):
    return floor(float*10**digits)/10**digits


def getCaptchaToken(siteKey, siteUrl):
    keys = loadKeys()['dbc']
    username = keys['username']
    password = keys['password']
# Put the proxy and reCaptcha token data
    Captcha_dict = {
        'proxytype': 'HTTP',
        'googlekey': siteKey,
        'pageurl': siteUrl}

    json_Captcha = json.dumps(Captcha_dict)

    client = deathbycaptcha.HttpClient(username, password)

    try:
        # balance = client.get_balance()
        # Put your CAPTCHA type and Json payload here:
        captcha = client.decode(type=4, token_params=json_Captcha)
        if captcha:
                # The CAPTCHA was solved; captcha["captcha"] item holds its
            # numeric ID, and captcha["text"] item its a text token".
            return captcha["text"]

            if '':  # check if the CAPTCHA was incorrectly solved
                client.report(captcha["captcha"])
    except deathbycaptcha.AccessDeniedException:
        # Access to DBC API denied, check your credentials and/or balance
        print("error: Access to DBC API denied, check your cred or balance")

botMsg = createTelegramBot()
