import logging
import asyncio
import concurrent.futures
from util import makeClient
from exchangeClients.rawAPIs.coinFalcon import ResponseError
import json
from order import Order
import exceptions
import selenium.common.exceptions


class MarketController:
    # This class controls the view and actions on the weak exchange
    def __init__(self, configFile, keyFile=None):
        DEFAULT_CONFIG = "../configs/default.json"
        self.configFile = configFile
        self._loadConfig(DEFAULT_CONFIG)
        self._loadConfig(self.configFile)
        logging.info(str(vars(self)))

        self.AAA = self.market[:3]
        self.BBB = self.market[-3:]

        if keyFile is not None:
            self.client = makeClient(
                self.exchangeKeyRef, self.exchangeAPIRef, keyFile, self.market
            )
        else:
            logging.warning("No key file provided, no client made")
        # These will change during normal operation

        self.processedTrades = []
        self.bookPair = {}  # The pair of order books (buy/sell)
        self.trackedOrders = {  # Attempts to track the orders we have open
            "buy": self.createPreOrder("buy"),
            "sell": self.createPreOrder("sell"),
        }
        self.orders = []  # The orders that open according to the exchange API
        self.balancesAAA = None
        self.balancesBBB = None

    def _loadConfig(self, file):
        # Example config:
        # {
        #     "configName": "cfETH",
        #     "exchangeKeyRef": "coinfalcon",
        #     "exchangeAPIRef": "coinfalcon",

        #     "exchangeName": "coinfalcon",
        #     "market": "ETH-BTC",
        #     "precision": 5,    // number of significant digits the exchange uses for prices
        #     "fee": 0.0025,     // fee that the weak exchange takes on filled orders
        #     "targetDepth": 0.001,  // How deep we want our orders to be (in BTC)
        #     "volumeRatio": 0.99,  // Ratio of  our balance we want to use in our order
        #     "undercut": 0.000011, // The amount to undercut (in BTC)
        #     "pRatioThreshold": 0.005, // Only place orders that are at least this profitable
        #     "minOrderSize": 10e-5,
        #     "btcUseRatio": 1 .      // How much of the balance on the strong exchange we are willing to use (may want to set this smaller than 1 in case there are simultaneous order fills)
        # }

        config = json.load(open(file))["exchange"]

        for key, value in config.items():
            setattr(self, key, value)
        # Should add check

    def maxAmount(self, side, price):
        """Returns the biggest order we can put down
        at a given price
        (measured in AAA)
        """
        if side == "sell":
            return self.volumeRatio * self.balancesAAA["total"]
        elif side == "buy":
            try:
                return self.volumeRatio * self.balancesBBB["total"] / price
            except ZeroDivisionError as e:
                logging.exception("")
                return 1

    def updateStep(self):
        self.updateState()
        if self.checkOrders:
            self.cancelUnusedOrders()
            self.deactivateNonExistentOrders()
        else:
            self.cancelUnusedOrders2()
            self.deactivateNonExistentOrders2()
        self.updateOrderDepths()

    # Updating info
    def updateState(self):
        if self.serial:
            self.updateStateSerial()
        else:
            self.updateStateParallel()

    def updateStateSerial(self):
        """Updates state with info from exchange API"""
        # Due to API rate limits, call this at most once per second
        logging.debug("Updating state serially...")
        self.updateBooks()
        self.updateBalances()
        self.updateOrders()
        self.getNewTrades()
        logging.debug("State successfully updated.")
        return True

    def updateStateParallel(self):
        """Updates state with info from exchange API"""
        # Due to API rate limits, call this at most once per second
        logging.debug("Updating state in parallel...")
        loop = asyncio.get_event_loop()
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=4)
        loop.run_until_complete(self._updateState(loop, executor))
        logging.debug("State successfully updated.")
        return True

    async def _updateState(self, loop, executor):
        await asyncio.wait(
            fs={
                loop.run_in_executor(None, self.updateBooks),
                loop.run_in_executor(None, self.updateBalances),
                loop.run_in_executor(None, self.updateOrders),
                loop.run_in_executor(None, self.getNewTrades),
            },
            return_when=asyncio.ALL_COMPLETED,
        )

    def updateBooks(self):
        """Updates order books for all markets"""
        logging.debug("Updating order books...")
        self.bookPair = self.client.getOrderBooks()

    def updateBalances(self):
        """'Updates Balances"""
        logging.debug("Updating balances..")
        balances = self.client.getBalances()
        self.balances = balances
        self.balancesAAA = balances[self.AAA]
        self.balancesBBB = balances[self.BBB]

    def updateOrders(self):
        logging.debug("Updating orders...")
        self.orders = self.client.getOpenOrders()

    def getNewTrades(self):
        newTrades = self.client.getNewTrades()

        self.processedTrades = []
        # Perhaps trade should just be in terms of gainLoss
        # Need a trade object like order object
        market = self.e2market
        for trade in newTrades:
            price = trade["price"]
            amount = trade["amount"]
            if trade["side"] == "sell":  # coinFalcon trades API is opposite
                processedTrade = {
                    "side": "sell",
                    "market": market,
                    "amount": amount,
                    "price": price,
                }
            elif trade["side"] == "buy":
                netAmount = amount * (1 - self.fee)
                processedTrade = {
                    "side": "buy",
                    "market": market,
                    "amount": netAmount,
                    "price": price,
                }
            processedTrade["exchange"] = self.configName
            self.processedTrades.append(processedTrade)

    def updateOrderDepths(self):
        for side, order in self.trackedOrders.items():
            order.depth = self.computeOrderDepth(order)["depth"]

    # Syncing exchange state and our state
    def cancelUnusedOrders(self):
        activeIds = [
            order.id for side, order in self.trackedOrders.items() if order.exists
        ]
        logging.info("active ids: " + str(activeIds))
        for exOrder in self.orders:
            if exOrder["id"] not in activeIds:
                logging.info("Cancelling {0} because not active".format(exOrder["id"]))
                order = self.createPreOrder(exOrder["side"])
                order.id = exOrder["id"]
                self.cancelOrder(order)

    def deactivateNonExistentOrders(self):
        exOrderIds = [exOrder["id"] for exOrder in self.orders]
        for side, order in self.trackedOrders.items():
            if order.id not in exOrderIds and order.exists:
                logging.info("Removing reference, order does not exist")
                order.deactivate()

    def cancelUnusedOrders2(self):
        for side, order in self.trackedOrders.items():
            if not order.exists:
                self.client.cancelOrder(order)

    def deactivateNonExistentOrders2(self):
        for exOrder in self.orders:
            self.trackedOrders[exOrder["side"]].price = exOrder["price"]
        exOrderSides = [exOrder["side"] for exOrder in self.orders]
        for side, order in self.trackedOrders.items():
            if (order.side not in exOrderSides) and order.exists:
                logging.info("Removing reference, order does not exist")
                order.deactivate()

    # Needed for the quoting engine
    def getTopPrice(self, side, target_depth):
        """
        Returns the top price in the book
        Ignores targetDepth volume, and our own order

        First removes our order from order book
        Returns stingiest price which has still has
        less than targetDepth before it.

        """
        depth = 0
        notOurPart = self.trackedOrders[side].notOurPart()
        for currentPrice, currentAmount in self.bookPair[side]:
            depth += notOurPart(currentPrice, currentAmount)
            if depth > target_depth:
                break
        return currentPrice

    def createPreOrder(self, side):
        """Returns an Order object that is pre-filled
        with correct fee, market and side"""
        preOrder = Order(self.market, side)
        for attribute in [
            "fee",
            "amountPrecision",
            "e2market",
            "pricePrecision",
            "configName",
            "minOrderSize",
        ]:
            setattr(preOrder, attribute, getattr(self, attribute))
        return preOrder

    # Placing and cancelling orders
    def placeOrder(self, order):
        if self.trackedOrders[order.side].exists:
            logging.warn(f"There is already an active {order.side} order")
            return

        # Send command to exchange
        logging.info(f"Attempting to place order {order}")
        try:
            orderResponse = self.client.placeOrder(order)
            order.id = orderResponse["id"]
        except exceptions.InsufficientBalanceError as e:
            logging.info(str(e))
        except (KeyError, TypeError) as e:
            logging.warn("Could not get order id")
            logging.info(str(e))
        else:
            order.exists = True
            logging.info(f"Placed {order}")
            self.trackedOrders[order.side] = order

    def cancelOrder(self, order):
        if order.id is None:
            logging.info(f"No order id: {order}")
            return None
        order.deactivate()
        try:
            self.client.cancelOrder(order)
        except exceptions.OrderDoesNotExistError as e:
            logging.info(str(e))
        except ResponseError as e:
            logging.exception(f"Failed to cancel order {e.message}")
        except selenium.common.exceptions.WebDriverException as e:
            logging.exception("")
        else:
            logging.info(f"Cancelled {order}")

    def computeOrderDepth(self, order):
        """Takes order object and computes its depth

        Returns:
        int entries: How far down the order book it is
        float depth: How much depth (in BTC) is above it
        """
        depth = 0
        counter = -1
        notOurPart = order.notOurPart()
        for currentPrice, currentAmount in self.bookPair[order.side]:
            counter += 1
            if order.appearsAbove(currentPrice):
                break
            else:
                depth += notOurPart(currentPrice, currentAmount)
        return {"entries": counter, "depth": depth}
