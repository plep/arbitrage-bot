from util import makeClient
import sys
from order import Order


keyFile = 'keyFile.json'
exchangeKeyRef = sys.argv[1]
exchangeAPIRef = sys.argv[1]

client = makeClient(exchangeKeyRef, exchangeAPIRef, keyFile, sys.argv[2])
rawClient = client.rawClient

smallOrder = Order(client.market, 'buy')
smallOrder.price = 0.02
smallOrder.amount = 0.001
smallOrder.exchangeName = exchangeAPIRef
# print(client.placeOrder(order))

bigOrder = Order(client.market, 'buy')
bigOrder.price = 0.02
bigOrder.amount = 100
bigOrder.exchangeName = exchangeAPIRef
